## Austria-Germany

It seems unfair to speak of these two as separate entities. Though throughout the game, many around them would seek to drive a wedge between them, the two nations kept a firm alliance through the entire game, sharing moves and schemes to form an unbreakable bond. Unsurprisingly, the benefits of having a border that didn&#39;t need defending led to a relatively easier game for both. Though they weren&#39;t totally unimpeded...

## Turkey

One of the most consistent thorns in Austria&#39;s side was Turkey. Sneaking behind enemy lines during retreats, they slowed down the Austrian advance by several years. Having once been allied to them, facing down the Russians side by side, the Austrians chose the perfect time to strike a devastating blow against the Turkish. But even then, having lost two supply at once and later having to disband three units in one turn, the Turks made it almost to the very end of the game and proved a worthy opponent.

## Russia

Russia, on the other hand, were as traitorous as Austria would later prove to be, only far less effective. Their short lived campaign largely revolved around promising something to Austria then immediately recanting that promise to no avail. Bad form.

## France

Another short lived campaign, but of an entirely different sort. France&#39;s demise was not of their own fault nor a sign of bad diplomacy, but only a tragic consequence of geopolitical realities beyond their control. England held the keys to their success but never offered them. Instead, Germany was able to use the English and Italians to block France in whilst Germany took all the spoils.

## England

Like the Turkish were to Austria, so the English to Germany. A trusting nation who sided with the Germans against the French and against the Russians. They saw the downfall of each and were absolutely instrumental to handing the Germans key supply centres. They, themselves, took much of Scandinavia and forged a sizeable empire across the north. Only to be struck through the heart by the German navy. Once the dagger was twisted and England lost its homeland, it&#39;s reckoning came swiftly.

## Italy

Italy did not die swiftly. Like Turkey, it held on to the last. Having once been part of an Austrian-German-Italian alliance, it had once shown great promise and possibly could have formed part of a triple victory. However, when Germany grew strong, whilst Austria and Italy grew only slowly, Italy began to sour. Maybe they had never intended to keep a triple alliance going, maybe it was fear of German treachery and betrayal, but the Italians broke eventually and suggested to Austria that they join together against the Germans. Horrified by such a suggestion, but not wholly unsurprised when it came, it was at that moment that Austria and Germany began seriously plotting the end of Italy.

And so it was that Austria and Germany remained true to their purpose and honourable in their victory. Many expected that one would turn on the other and take it all, but their glorious red/black empire will stand for evermore as a reminder that fellowship and trust can lead two nations to glory.

## Awards

Best Alliance: Austria-Germany

Worst Alliance: Austria-Turkey (Hon. Mention: Germany-England)

Most Trusting: England

Least Trusting: Italy

Sneakiest Retreat: Turkey

Quickest Backstab: Russia

Quickest Repeat Backstab: Russia

Best Scrubs Reference: Austria