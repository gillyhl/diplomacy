**Austria** – Dominant almost from the first year. Greatly helped by only really having to fight in one direction and by constant new builds coming in. Maybe stabbed Italy a little too early but did so very effectively. Was slowly inching their way towards victory before the betrayal led to the collapse of the coalition and a very impressive 19 supply centre win

**France** – Largely fought quite a static war. Looked in grave danger at the beginning but was able to recover to take hold of the battle against Germany and Italy. The Anglo-French alliance was vital to survival. Got caught with zero fleets which severely limited their ability to make much progress although remained a stalwart defender to the end which is reflected in a sneaky second place

**Germany** – What a recovery. Looked down and out after a failed war against the British and a Russian stab. Remarkable recovery due to a wandering Italian unit and the complete lack of British or Russian armies to finish you off. Somehow squeezed a supply centre from the British in a peaceful takeover and ended up in third place.

**Britain** – So loyal. So flawed. Strong start. Missed the opportunity to maybe crush the French and therefore remove their major rival on the board. Leaning on the French alliance meant a lack of obvious expansion opportunities for the British. Built all the fleets which was great for some things but left them completely unable to penetrate inland. Caused a lot of trouble by a sneaky Italian fleet which held them back as well. Deeply committed to the Coalition, ultimately to their own detriment. Very effective in stopping the Austrians in the Med but left wide open for a too easy stab

**Russia** – Just not loyal to anyone. Very much took a two fronts approach to the war which was very successful in Scandinavia but less so in the South. Ended up with split forces meaning he was easy prey for the advancing Austrians. Still retained a high number of centres till quite late. Made a fatal mistake in making a largely pathetic stab which opened him up to attacks from everyone. Last outpost in Edinburgh hilariously. Also, managed to work together with the British, Germans, Austrians and Turks and stab them all at least in some way.

**Italy** – Explorers first and foremost. I don&#39;t think anyone got themselves into weirder positions than Italy. That fleet in Britain was something else as was the army wandering around France and Germany. Fought hard but not particularly effectively against the French and left themselves so wide open for the stab which they knew was coming. Did not particularly deserve to die but did nonetheless.

**Turkey** – Just didn&#39;t go well. Early alliance of sorts with Austria was immediately broken. Alliance with Russia against the Austrians was quickly broken. Just left cornered and quickly destroyed by the Austrians.

## Awards

**Best player** – Austria. Obvs.

Runner-up – Germany

**Best alliance** – The Coalition

Genuinely could have stopped an Austrian victory if they had stayed together

**Best stab** – Austria on Italy

From 5 centres to eliminated in two years

**Rob Whitehorn award for terrible first move**

Russia. Only country not to gain a centre. Very nearly France

**Moves of the game**

Austria&#39;s three build first turn

Britain&#39;s takeover of the Mediterranean

The Anglo-Russian destruction of Germany

**Fail of the game**

All of the countries who only had armies or fleets

Russia&#39;s stab

**Best press**

All the Austria as Voldemort stuff. All of the German stuff



**Closing thoughts**

This game was dominated by two largely separate spheres for the majority of the game. There was a clear victor on one side and there wasn&#39;t on the other. This ultimately decided the game. It seemed to largely well played with interesting moves and frustrations for all I think.