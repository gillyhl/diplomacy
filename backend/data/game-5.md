# Winner’s History Game 5

**Russia**:  On the back of some comprehensive first term negotiations, and a little bit of luck, they found themselves on seven supplies after the first year. From there they were careful not to rise too quickly. Seized a fantastic opportunity and made a well-timed mid-game stab that opened the door to a possible solo.  Fought a careful late game, not taking any risks and making steady gains wearing down the opposition coalition. Didn’t manage the 19 supply of Austria’s solo in game 1, but did achieve it at an earlier date. 

**Italy**: Played a very good early game, managed to successfully convince Russia that they should gain territory at an equal pace; but for the second time in a row the Italian starting position proved too vulnerable to a well-timed stab. Late game they superbly led the coalition, making key gains at important points, but never quite recovered from the divisive nature of Russia’s stab. Being cut off from their Turkish gains was a deathblow, albeit a slow one. Deserved the second place and unlucky not to be part of a successful stalemate. 

**England**: Russia’s first ally, and organiser of the ‘Northern Powers’ who all agreed to march South. This proved beneficial to all parties, but ultimately failure to pressure Russia in the North until late in the game allowed them to progress too fast. Nevertheless, played a very good late game and caused plenty of problems, but perhaps too cautious? A last turn assault on STP may even have resulted in a stalemate. Very honest, very honourable and deserved third place. 

**Germany**: The third and final member of the late game coalition played a good early game as one of the Northern Powers. Teaming up with England to attack France. Ultimately though, the defeat of France was too slow and as the Russo-Italians defeated Turkey, Germany turned on the greatest ally, thus meaning poor Germany had to fight on two fronts. Made one or two fatal decisions in the late game coalition and lost any chance of second or third. However they were loyal in the end and turned down repeated opportunities to stab Italy.

**France**: Struggled from the start due to an Anglo-German alliance and an Eastward looking Italy. Made a friend in Austria out of desperation to find an ally. Did well to survive as long as they did, and was perhaps unfairly denied a chance to prove themselves in the coalition. 
Turkey: “There were three in the bed and the little one said rollover” You can tell I’ve just put my son to bed, but this is apt. The East simply wasn’t big enough for three, their failings came from failing to convince either Italy or Russia that they were indispensable. They trusted Italy, who were feeding Russia intel the whole time. Perhaps another case of paying for the sins of the forefathers? 

**Austria**: Entered the cafeteria late to find all the tables taken up with ready made alliances, quickly surveyed the room and was about to walk straight out. As they were leaving France called out from the corner that there was a wonky stool named Munich they could perch on. Unfortunately, it collapsed very soon after. (Never stood a chance after clearly not realising the game had started!)

## Awards
**Best alliance**: The coalition – made life difficult, made mistakes, but stuck together, no wavering even as the tide turned, unlike coalitions in past games.

**Worst alliance**:  Franco-Austrian – Austria eliminated and France left alone…
Best stab: Russia on Italy. – Only took 2 supplies, but divided to conquer.

**Worst Stab**: Germany on England: The decision to stab England was the catalyst for Russia to go for a solo victory. (So, Italy attempting to manipulate Germany into stabbing England, inadvertently found themselves causing their own stabbing courtesy of Russia.)

**Best Press**: Russia-Italy-Turkey. The whole debacle about who and where stabs would be on. There were stabs on Russia, Italy and Turkey that were being planned simultaneously! HIGHLIGHT: Russia approached Turkey to stab Italy (when really they were about to stab Turkey and wanted the faked trust). Turkey told Italy about Russia’s plans. Italy informed Russia that they had been ‘snitched on’. No one knew who to trust.

**Honourable Mention**: How on earth did France convince Austria to invade Mun instead of defending their homelands??? Must have been one heck of a sales pitch!
Worst Press: Russia to England: Tried to copy a paste a select amount of press that would sow mistrust between the coalition. COPIED A WHOLE THREAD BY MISTAKE. Including detailed conversations about how to stab England from Russia. Doh! 

### Best moves
* Russia’s first year. (7 Supplies!)
* Austria’s Munich holiday.
* Italian capture of Greece and Trieste.
* Russia’s 2 turn plan of capturing Den to conquer Kie. 

### Closing thoughts
Russia had been the lowest performing country up until this point and then scored the quickest solo.
A coalition that stays together does not always succeed. Constant communication is needed.
Only two games left!
 