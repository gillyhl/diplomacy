**Turkey** – Got off to a slow start, not claiming a big enough piece of Austria’s carcass. However an alliance with Italy formed that led to Russia being pushed up to Scandinavia. Of all the many stabs of Italy this game, Turkey’s was the most successful and the most devastating seen in any game so far taking 5 SC’s in one turn. From that point a solo looked on the cards but Russia’s obstinance led to them settling for a joint victory.

**France** – In the early years they were never really threatened in the south, allowing them to embark on a highly successful campaign quickly encompassing Germany and England. Frequent and largely successful stabs led to France always looking in a good position but the bad reputation that came with these stabs meant that Russia and Italy were against them more than they were for them. This meant they had to settle for second place behind Turkey

**Russia** – Got too big too soon. On 8 SCs after 2 years forced Italy and Turkey to team up against them forcing them to retreat to Scandinavia where they largely stayed for rest of the game. They spent the remainder of the game joining various coalitions against either Turkey or France depending on who was looking more likely to get a solo. Like Italy they can be applauded for achieving this. 

**Italy** – Played a very strong early and middle game, dispatching Austria quickly and gaining ground in Central Europe. Sadly they proved too trusting of their allies and left themselves open to multiple stabbings from France and Turkey. Unfortunate not to make it to the end but should be proud that they achieved their goal of not allowing anyone else a solo.

**Britain** – Had a solid start but lost the Scandinavian battle to Russia and were betrayed by France. Did an impressive amount of damage with just two fleets for a while but were inevitably destroyed.
Germany – Never really gained much of a foothold on the map, were quickly beaten down by France and England. One of this games refugees ended up being housed in in Poland for a while until Turkey and Italy had no further use for them. 

**Austria** – Very nearly our fastest ever death, with immediate attacks from its 3 neighbours. However they impressively managed to escape to Munich where they were kept as a pet by France for several years. Their unwillingness to grant other refugees sanctuary eventually led to their downfall.

## Awards
**Best alliance** – Russia and Italy against a solo
In a game with such a wayward moral centre it was difficult to give this award out without insulting previous winners. 

**Best stab** – Turkey on Italy

**Most Stabbed Nation** - Italy

**Best Refugee Nation** – Austria (Hon. Mention: Germany)

## Closing thoughts
This was a game defined by backstabbing so it is fitting that the 2 victors were by far the most dishonourable players. Let us move on from this drag of a game and try and redeem our reputations in future games.