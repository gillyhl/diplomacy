module.exports = {
  mongodb: {
    host: process.env.MONGODB_URI
  },
  google_oauth: {
    clientId: process.env.GOOGLE_OAUTH_CLIENT_ID,
    clientSecret: process.env.GOOGLE_OAUTH_CLIENT_SECRET,
    redirect: process.env.GOOGLE_OAUTH_REDIRECT
  },
  jwt: {
    secret: process.env.JWT_SECRET
  }
}