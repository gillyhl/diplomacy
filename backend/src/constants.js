const chalk = require('chalk')
const COUNTRIES = Object.freeze({
  FRANCE: {
    text: 'FRANCE',
    style: chalk.bold.bgCyan
  },
  BRITAIN: {
    text: 'BRITAIN',
    style: chalk.bold.bgBlue
  },
  TURKEY: {
    text: 'TURKEY',
    style: chalk.bold.bgYellow
  },
  AUSTRIA: {
    text: 'AUSTRIA',
    style: chalk.bold.bgRed
  },
  ITALY: {
    text: 'ITALY',
    style: chalk.bold.bgGreen
  },
  GERMANY: {
    text: 'GERMANY',
    style: chalk.bold.bgBlack
  },
  RUSSIA: {
    text: 'RUSSIA',
    style: chalk.bold.bgMagenta
  }
})

const SEASON = Object.freeze({
  SPRING: {
    text: 'SPRING',
    value: 1
  },
  SUMMER: {
    text: 'SUMMER',
    value: 2
  },
  AUTUMN: {
    text: 'AUTUMN',
    value: 3
  },
  WINTER: {
    text: 'WINTER',
    value: 4
  }
})

const getSeasonValue = season => {
  const seasonEnum = SEASON[season]

  return seasonEnum && seasonEnum.value
}

const DEFAULT_POSITION_BONUS = [32,16,8,4,2,1,0]

const COUNTIRES_BALANCE = Object.freeze({
  [COUNTRIES.FRANCE.text]: -2,
  [COUNTRIES.BRITAIN.text]: -1,
  [COUNTRIES.RUSSIA.text]: -1,
  [COUNTRIES.TURKEY.text]: -1,
  [COUNTRIES.GERMANY.text]: 0,
  [COUNTRIES.AUSTRIA.text]: 1,
  [COUNTRIES.ITALY.text]: 2
}) 

const DEFAULT_MAX_YEARS_BONUS = 12

const DEFAULT_POINTS_PER_FULL_YEAR = 0.5

const START_YEAR = 1901

const POINTS_PER_SUPPLY_CENTRE = 1

const MAX_PLAYERS = 7

const WINNING_SC_COUNT = 18
const TOTAL_SC_COUNT = 34

module.exports = {
  COUNTRIES,
  COUNTIRES_BALANCE,
  DEFAULT_POINTS_PER_FULL_YEAR,
  DEFAULT_MAX_YEARS_BONUS,
  DEFAULT_POSITION_BONUS,
  SEASON,
  START_YEAR,
  POINTS_PER_SUPPLY_CENTRE,
  MAX_PLAYERS,
  WINNING_SC_COUNT,
  TOTAL_SC_COUNT,
  getSeasonValue
}