const {
  COUNTIRES_BALANCE,
  DEFAULT_MAX_YEARS_BONUS,
  DEFAULT_POINTS_PER_FULL_YEAR,
  START_YEAR,
  DEFAULT_POSITION_BONUS,
  POINTS_PER_SUPPLY_CENTRE,
  WINNING_SC_COUNT,
  TOTAL_SC_COUNT
} = require("../constants");
const _ = require("lodash");
const preProcessJson = require("./processJson");
const { addRankingToResults } = require("./ranking");
const { createConnection } = require("../repository/db");
const { model: playerModel } = require("../repository/models/Player");

const scoreJson = json => {
  const results = preProcessJson(json);

  const resultsWithRank = addRankingToResults(results);
  return scoreGame(resultsWithRank);
};

const scoreGame = (results, options = {}) => {
  const countryDidSolo = v => v.gameStats.supplyCentres >= WINNING_SC_COUNT;

  const gameHasSolo = _.chain(results)
    .some(countryDidSolo)
    .value();

  return _.chain(results)
    .map(v => {
      const scoring = scoreCountry(
        {
          country: v.country,
          ...v.gameStats,
          rank: v.meta.rank,
          rankSharedWith: v.meta.rankSharedWith,
          didSolo: countryDidSolo(v)
        },
        options,
        gameHasSolo
      );
      return {
        ...v,
        meta: {
          ...v.meta,
          score: scoring.score,
          scoringBreakdown: scoring.breakdown
        }
      };
    })
    .orderBy([o => o.meta.score], ["desc"])
    .map((v, i, array) => ({
      ...v,
      meta: {
        ...v.meta,
        overallRank:
          _.findIndex(array, vi => v.meta.score === vi.meta.score) + 1
      }
    }))
    .value();
};

const scoreCountry = (
  { country, supplyCentres, time, rank, rankSharedWith, didSolo },
  {
    pointsPerFullYear = DEFAULT_POINTS_PER_FULL_YEAR,
    maxYearsBonus = DEFAULT_MAX_YEARS_BONUS,
    pointsPerSupplyCentre = POINTS_PER_SUPPLY_CENTRE,
    positionBonus = DEFAULT_POSITION_BONUS
  },
  gameHasSolo
) => {
  const balance = COUNTIRES_BALANCE[country];
  const fullYears = time.year - START_YEAR;
  const fullYearsPoints =
    (fullYears > maxYearsBonus || supplyCentres ? maxYearsBonus : fullYears) *
    pointsPerFullYear;
  const positionBonusPoints =
    Math.floor(
      (100 *
        _.chain(positionBonus)
          .slice(rank - 1, rank + rankSharedWith - 1)
          .sum()
          .value()) /
        rankSharedWith
    ) / 100;
  const supplyCentresScore = gameHasSolo
    ? didSolo
      ? TOTAL_SC_COUNT * pointsPerSupplyCentre
      : 0
    : supplyCentres * pointsPerSupplyCentre;
  return {
    score: balance + supplyCentresScore + fullYearsPoints + positionBonusPoints,
    breakdown: {
      balance,
      supplycentres: supplyCentresScore,
      fullYears: fullYearsPoints,
      position: positionBonusPoints
    }
  };
};

const processScoreForTournament = async scores => {
  const connection = createConnection();
  const Player = playerModel(connection);

  const result = await Promise.all(
    scores.map(async score => {
      const player = await Player.findOne({ backstabbrTag: score.player });

      return {
        player,
        rank: score.meta.overallRank,
        score: score.meta.score,
        country: score.country,
        meta: score.meta.scoringBreakdown
      };
    })
  );

  connection.close();
  return result;
};

module.exports = {
  scoreJson,
  processScoreForTournament
};
