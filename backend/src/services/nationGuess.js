const { createConnection } = require('../repository/db')
const { model: nationGuessModel } = require('../repository/models/NationGuessGame')
const { model: tournamentModel } = require('../repository/models/Tournament')
const { model: playerModel } = require('../repository/models/Player')
const CryptoJS = require('crypto-js')
const { ObjectId } = require('mongoose').Types

const createNationGuessGame = async (tournamentId, name, playerId) => {
  const connection = createConnection()
  const NationGuessGame = nationGuessModel(connection)
  const Tournament = tournamentModel(connection)
  const Player = playerModel(connection)

  try {
    const tournament = await Tournament.findById(tournamentId)
    const gameCreator = await Player.findById(playerId)
    if (!tournament) {
      throw Error
    }
    
    return await NationGuessGame.create({
      tournament,
      gameCreator,
      name
    })
  } catch (e) {
    console.error(e.errmsg)
  } finally {
    connection.close()
  }
}

const getNationGuessingGames = async (userId) => {
  const connection = createConnection()
  const NationGuessGame = nationGuessModel(connection)

  try {
    return await NationGuessGame.find({ 'tournament.players._id': userId }, {
      'tournament._id': 1,
      'tournament.name': 1,
      'gameCreator._id': 1,
      name: 1,
      locked: 1
    }).sort('-_id')
  } catch (e) {
    console.error(e.errmsg)
  } finally {
    connection.close()
  }
}

const getNationGuessingGameById = async (id, userId) => {
  const connection = createConnection()
  const NationGuessGame = nationGuessModel(connection)

  try {
    return await NationGuessGame.findOne({ _id: id, 'tournament.players._id': userId }, {
      'guesses.password': 0
    }).sort('-_id')
  } catch (e) {
    console.error(e.errmsg)
  } finally {
    connection.close()
  }
}

const queryPlayersInNationGuessingGame = async (id, query) => {
  const connection = createConnection()
  const NationGuessGame = nationGuessModel(connection)

  try {
    const result = await NationGuessGame.aggregate()
      .match({ _id: ObjectId(id) })
      .unwind('$tournament.players')
      .match({
        $or: [
          { 'tournament.players.realName': { $regex: new RegExp(`${query}`), $options: 'i' }},
          { 'tournament.players.backstabbrTag': { $regex: new RegExp(`${query}`), $options: 'i' }}
        ]
      })
      .replaceRoot('tournament.players')
    return result
  } catch (e) {
    console.error(e.errmsg)
  } finally {
    connection.close()
  }
}

const deleteNationGuessingGame = async (id, userId) => {
  const connection = createConnection()
  const NationGuessGame = nationGuessModel(connection)

  try {
    return await NationGuessGame.delete({ _id: id, 'tournament.players._id': userId })
  } catch (e) {
    console.error(e.errmsg)
  } finally {
    connection.close()
  }
}

// TODO: Only allow game owner to lock
const lockNationsGuessingGame = async (id, userId) => {
  const connection = createConnection()
  const NationGuessGame = nationGuessModel(connection)

  try {
    return await NationGuessGame.updateOne({ _id: id, 'tournament.players._id': userId }, { locked: true })
  } catch (e) {
    console.error(e.errmsg)
  } finally {
    connection.close()
  }
}

const addGuessToGame = async (gameId, playerId, guess, password) => {
  const connection = createConnection()
  const NationGuessGame = nationGuessModel(connection)
  const Player = playerModel(connection)
  const encryptedGuess = CryptoJS.AES.encrypt(guess, password).toString()
  const hashedPassword = CryptoJS.SHA256(password).toString()
  try {
    const player = await Player.findById(playerId)
    const result = await NationGuessGame.updateOne({ _id: gameId, 'guesses.player._id': playerId }, {
      $set: {
        'guesses.$': {
          player,
          encryptedGuess: encryptedGuess,
          password: hashedPassword
        }
      }
    })

    if (!result.nModified) {
      await NationGuessGame.updateOne({ _id: gameId }, {
        $push: {
          'guesses': {
            player,
            encryptedGuess: encryptedGuess,
            password: hashedPassword
          }
        }
      })
    }
  } catch (e) {
    console.error(e.errmsg)
  } finally {
    connection.close()
  }
}

const decryptGuess = async (gameId, playerId, password) => {
  const connection = createConnection()
  const NationGuessGame = nationGuessModel(connection)
  const hashedPassword = CryptoJS.SHA256(password).toString()
  try {
    const matchQuery = { _id: ObjectId(gameId), 'guesses.player._id': ObjectId(playerId), 'guesses.password': hashedPassword }
    const result = await NationGuessGame.aggregate()
    .unwind('$guesses') 
    .match(matchQuery)
    .replaceRoot('guesses')
    
    if (!result.length) {
      throw new Error('No matching password and player combonation')
    }
    
    const encryptedGuess = result[0].encryptedGuess
    const decryptedGuess = CryptoJS.AES.decrypt(encryptedGuess, password).toString(CryptoJS.enc.Utf8)

    await NationGuessGame.updateOne(matchQuery, {
      $set: {
        'guesses.$.decryptedGuess': decryptedGuess
      }
    })
    
  } catch (e) {
    console.error(e.message || e.errmsg)
    throw(e)
  } finally {
    connection.close()
  }
}

module.exports = {
  createNationGuessGame,
  getNationGuessingGames,
  deleteNationGuessingGame,
  lockNationsGuessingGame,
  getNationGuessingGameById,
  queryPlayersInNationGuessingGame,
  addGuessToGame,
  decryptGuess
}