const _ = require('lodash')
module.exports = json => _.map(json, (v, k) => ({
  ...v,
  country: k,
  meta: {}
}))