const { createConnection } = require('../repository/db')
const { model: tournament } = require('../repository/models/Tournament')
const fs = require('fs')

const getGameById = async (id, userId) => {
  const connection = createConnection()
  const Tournament = tournament(connection)

  try {
    const t = await Tournament.findOne(
      {'games._id': id, 'players._id': userId},
      { 'games': { $elemMatch: { _id: id } } }
    )

    return t.games[0]

  } catch (e) {
    console.error(e.errmsg)
    throw e
  } finally {
    connection.close()
  }
  
}

const addHistoryToGame = async (gameId, historyFilePath) => {
  const connection = createConnection()
  const Tournament = tournament(connection)
  try {
    await Tournament.updateOne(
      {'games._id': gameId},
      {
        'games.$.history': fs.readFileSync(require('path').resolve(historyFilePath))
      }

    )
  } catch (e) {
    console.error(e.errmsg)
    throw e
  } finally {
    connection.close()
  }
}

module.exports = {
  getGameById,
  addHistoryToGame
}