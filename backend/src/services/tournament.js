const { createConnection } = require('../repository/db')
const { model: tournament } = require('../repository/models/Tournament')
const _ = require('lodash')
const NotFoundError = require('../errors/NotFound')

const createTournament = async tournamentBody => {
  const connection = createConnection()
  const Tournament = tournament(connection)

  try {
    await Tournament.create(tournamentBody)
  } catch (e) {
    console.error(e.errmsg)
    throw e
  } finally {
    connection.close()
  }

}

const findTournament = async name => {
  const connection = createConnection()
  const Tournament = tournament(connection)

  try {
    const foundTournaments = await Tournament.find({ name })
    return foundTournaments
  } catch (e) {
    console.error(e.errmsg)
    throw e
  } finally {
    connection.close()
  }
}

const listTournaments = async (userId, { from = 0, size = 10 }) => {
  const connection = createConnection()
  const Tournament = tournament(connection)

  try {
    return await Tournament.find({ 'players._id': userId }, { games: 0 })
  } catch (e) {
    console.error(e.errmsg)
    throw e
  } finally {
    connection.close()
  }
}

const getTournamentById = async (id, userId) => {
  const connection = createConnection()
  const Tournament = tournament(connection)

  try {
    const tournament = await Tournament.findOne({ _id: id, 'players._id': userId }).lean()
    if (tournament) {
      return {
        ...tournament,
        standings: getTournamentStandings(tournament)
      }
    }
    return tournament
  } catch (e) {
    console.error(e.errmsg)
    throw e
  } finally {
    connection.close()
  }
}

const getTournamentStandings = tournament => {
  const initialPlayerScores = _.reduce(tournament.players,  (a, c) => ({
    ...a,
    [c._id]: {
      score: 0,
      player: c
    }
  }), {})

  const scores = getScores(tournament, initialPlayerScores)
  return addRankToScores(scores)

  
}

const addRankToScores = scores => {
  const orderedScores = _.chain(scores)
    .orderBy(['score'], ['desc'])
    .value()
  
  return _.chain(scores)
    .mapValues((v, k) => ({
      ...v,
      id: k,
      rank: _.findIndex(orderedScores, vi => v.score === vi.score) + 1
    }))
    .values()
    .orderBy(['rank'])
    .value()
}

const getScores = (tournament, initialPlayerScores) => 
  _.reduce(tournament.games, (a, c) => ({
    ...a,
    ..._.reduce(c.results, (ai, c) => ({
      ...ai,
      [c.player._id]: {
        ...ai[c.player._id],
        score: (ai[c.player._id].score || 0) + c.score
      }
    }), a)
  }), initialPlayerScores)

const addGameToTournament = async ({ _id }, game) => {
  const connection = createConnection()
  const Tournament = tournament(connection)

  try {
    await Tournament.findOneAndUpdate({ _id}, { $push: { games: game }})
  } catch (e) {
    console.error(e.errmsg)
    throw e
  } finally {
    connection.close()
  }
}

const findTournamentsByName = async name => {
  const connection = createConnection()
  const Tournament = tournament(connection)

  try {
    const tournaments = await Tournament.find({
      name: { $regex: new RegExp(`${name}`), $options: 'i' }
    }, {
      _id: 1,
      name: 1
    })

    return tournaments
  } catch (e) {
    console.error(e.errmsg)
    throw e
  } finally {
    connection.close()
  }
}

const getTournamentStats = async (id, userId) => {
  const tournament = await getTournamentById(id, userId)
  if (!tournament) throw new NotFoundError()
  return {
    pointsPerCountry: getPointsPerCountry(tournament)
  }
}

const getPointsPerCountry = tournament => {
  const scores = _.chain(tournament.games)
    .reduce(
      (acc, cur) => ({
        ...acc,
        ..._.chain(cur.results)
          .keyBy('country')
          .mapValues((v, k) => v.score + (acc[k] || 0))
          .value()
      }),
      {}
    )
    .mapValues((v, k) => ({
      country: k,
      score: v
    }))
    .value()

  return addRankToScores(scores)
}

  
module.exports = {
  createTournament,
  findTournament,
  getTournamentStandings,
  addGameToTournament,
  listTournaments,
  getTournamentById,
  findTournamentsByName,
  getTournamentStats
}

