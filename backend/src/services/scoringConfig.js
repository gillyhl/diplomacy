const { createConnection } = require('../repository/db')
const { model: scoringConfig } = require('../repository/models/ScoringConfig')

const createNewScoringConfig = async config => {
  const connection = createConnection()
  const ScoringConfig = scoringConfig(connection)

  try {
    await ScoringConfig.create(config)
  } catch (e) {
    console.error(e.errmsg)
  }

  connection.close()
}

const findOneScoringConfig = async name => {
  const connection = createConnection()
  const ScoringConfig = scoringConfig(connection)

  try {
    const configs = await ScoringConfig.findOne({ name })
    connection.close()
    return configs
  } catch (e) {
    console.error(e.errmsg)
  }

}

module.exports = {
  createNewScoringConfig,
  findOneScoringConfig
}