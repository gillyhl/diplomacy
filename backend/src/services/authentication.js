const { google_oauth } = require('../config')
const { google } = require('googleapis')
const jwt_decode = require('jwt-decode')
const { createConnection } = require('../repository/db')
const { model: playerModel } = require('../repository/models/Player')
const config = require('../config')
const jwt = require('jsonwebtoken')

const createOauthClient = () => new google.auth.OAuth2(
  google_oauth.clientId,
  google_oauth.clientSecret,
  google_oauth.redirect
)

const scope = ['profile', 'email']

const getAuthUrl = oauth2Client => oauth2Client.generateAuthUrl({
  access_type: 'offline',
  scope,
  prompt: 'consent'
})

const getTokens = async (oauth2Client, code) => oauth2Client.getToken(code)

const getUserInformation = tokens => {
  if (tokens.id_token) return jwt_decode(tokens.id_token)
  return null
}

const getUserEmailFromToken = tokens => {
  return getUserInformation(tokens).email
}

const verifyToken = (token, client) => client.verifyIdToken({
  idToken: token,
  audience: google_oauth.clientId
})

const getUpdatedPlayerData = (payload, backstabbrTag) => ({
  backstabbrTag, 
  realName: payload.given_name,
  email: payload.email
})

const updatePlayer = async (payload, backstabbrTag) => {
  const connection = createConnection()
  const Player = playerModel(connection)
  try {
    return await Player.findOneAndUpdate({ backstabbrTag }, getUpdatedPlayerData(payload, backstabbrTag), { new: true })
  } catch (e) {
    console.error(e.errmsg)
  } finally {
    connection.close()
  }
}

const registerUser = async (decodedToken, { backstabbrTag }) => {
  const { payload } = decodedToken
  const connection = createConnection()
  const Player = playerModel(connection)
  try {
    const player = await Player.findOne({ backstabbrTag })
    if (player) {
      return updatePlayer(payload, backstabbrTag)
    }

    return await Player.create(getUpdatedPlayerData(payload, backstabbrTag))
  } catch (e) {
    console.error(e.errmsg)
  } finally {
    connection.close()
  }
}

const getJwtForPlayer = ({ realName, backstabbrTag, email, id }) => jwt.sign(
  { realName, backstabbrTag, email },
  config.jwt.secret,
  {
    expiresIn: '1 day',
    subject: id,
    issuer: 'diplomacy.gchl.uk'
  })

module.exports = {
  createOauthClient,
  getAuthUrl,
  getTokens,
  getUserInformation,
  getUserEmailFromToken,
  verifyToken,
  registerUser,
  getJwtForPlayer
}