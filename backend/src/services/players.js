const { createConnection } = require('../repository/db')
const { model: playerModel } = require('../repository/models/Player')
const mongoose = require('mongoose')
const createNewPlayer = async ({ realName, backstabbrTag }) => {
  const connection = createConnection()
  const Player = playerModel(connection)
  try {
    await Player.create({ realName, backstabbrTag })
  } catch (e) {
    console.error(e.errmsg)
  }

  connection.close()
}

const findPlayers = async input => {
  const connection = createConnection()
  const Player = playerModel(connection)

  try {
    const players = await Player.find({ $or: [{backstabbrTag: input}, {realName: input}] })
    return players
  } catch (e) {
    console.error(e.errmsg)
  } finally {
    connection.close()
  }
}

const findPlayerByEmail = async email => {
  const connection = createConnection()
  const Player = playerModel(connection)

  try {
    const player = await Player.findOne({ email })
    return player
  } catch (e) {
    console.error(e.errmsg)
  } finally {
    connection.close()
  }
}

module.exports = {
  createNewPlayer,
  findPlayers,
  findPlayerByEmail
}