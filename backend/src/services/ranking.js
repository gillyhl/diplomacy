const _ = require('lodash')
const { SEASON, getSeasonValue } = require('../constants')

const isTie = sortingFunction => (a,b) => {
  return sortingFunction(a, b) === 0
}

const sortResults = ({gameStats: a}, {gameStats: b}) => {
  const aSeasonValue = getSeasonValue(a.time.season)
  const bSeasonValue = getSeasonValue(b.time.season)
  if (a.time.year > b.time.year) {
    return -1
  }

  if (a.time.year < b.time.year) {
    return 1
  }

  if (aSeasonValue > bSeasonValue) {
    return -1
  }

  if (aSeasonValue < bSeasonValue) {
    return 1
  }

  if (a.supplyCentres > b.supplyCentres) {
    return -1
  }

  if (a.supplyCentres < b.supplyCentres) {
    return 1
  }

  return 0
}

const addRankingToResults = results => {
  const ranks = rankResults(results, sortResults)
  return results.map((v, i) => ({
    ...v,
    meta: {
      ...v.meta,
      rank: ranks[i],
      rankSharedWith: ranks.filter(rank => ranks[i] === rank).length
    }
  }))
}

const rankResults = (resultsArray, sortingFunction) => {
  const sortedArray = [...resultsArray].sort(sortingFunction)
  const isTieFn = isTie(sortingFunction)
  return resultsArray.map(result => _.findIndex(sortedArray, sortedResult => isTieFn(result, sortedResult)) + 1)
}

module.exports = {
  addRankingToResults
}