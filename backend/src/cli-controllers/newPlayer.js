const inquirer = require('inquirer')
const { createNewPlayer } = require('../services/players')

module.exports = async () => {
  const answers = await inquirer.prompt([
    {
      name: 'backstabbrTag',
      message: 'What is the Backstabbr tag?',
      validate: async name => name.length !== 0 || 'You must enter a value'
    },
    {
      name: 'realName',
      message: 'What is the real name of the player?',
      validate: async name => name.length !== 0 || 'You must enter a value'
    }
  ])
  createNewPlayer(answers)
  
}