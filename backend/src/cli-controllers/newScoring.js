const inquirer = require('inquirer')
const { MAX_PLAYERS } = require('../constants')
const { createNewScoringConfig } = require('../services/scoringConfig')

module.exports = async () => {
  const answers = await inquirer.prompt([
    {
      name: 'name',
      message: 'What do you want to call the scoring system?',
      filter: value => value.trim(),
      validate: value => !!value.length
    },
    {
      name: 'pointsPerFullYear',
      message: 'How many points per year do you want to award?',
      default: 0,
      filter: value => parseFloat(value),
      validate: value => !isNaN(parseFloat(value))
    },
    {
      name: 'maxYearBonus',
      message: 'How many years do you want award a year bonus for?',
      default: 0,
      filter: value => parseInt(value),
      validate: value => !isNaN(parseInt(value))
    },
    {
      name: 'pointsPerSupplyCentre',
      message: 'How many points do you want to award for each supply centre?',
      default: 0,
      filter: value => parseFloat(value),
      validate: value => !isNaN(parseFloat(value))
    },
    {
      name: 'positionBonus',
      message: 'What points do you want to award for wach position',
      default: [32,16,8,4,2,1],
      filter: value => {
        const parsedValues = value.split(',').map(v => parseFloat(v))
        const zeroArrayLength = (MAX_PLAYERS - parsedValues.length) > 0 ? MAX_PLAYERS - parsedValues.length : 0
        return [ ...parsedValues, ...new Array(zeroArrayLength).fill(0) ]
      },
      validate: value => value.every(v => !isNaN(v))
    }
  ])
  
  createNewScoringConfig(answers)
  
}