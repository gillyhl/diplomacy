
/**
 * TODO:
 * 1. Make sure no duplicate players are added to the players array to add.
 * 2. Allow for a maximum of 7 players to be added.
 */

const inquirer = require('inquirer')
const { findPlayers } = require('../services/players')
const { findOneScoringConfig } = require('../services/scoringConfig')
const { createTournament } = require('../services/tournament')
const chalk = require('chalk')

const wantsMorePlayers = async () => {
  const { morePlayers } = await inquirer.prompt([
    {
      name: 'morePlayers',
      message: 'Do you wish to add more players',
      type: 'list',
      choices: ['yes', 'no']
    }
  ])

  return morePlayers === 'yes'
}

const getPlayers = async (currentPlayers) => {
  if (currentPlayers.length) {
    console.log(`Currently selected players: ${currentPlayers.map(p => chalk.green(p.backstabbrTag)).join(', ')}`)
  }
  const { playerName } = await inquirer.prompt([
    {
      name: 'playerName',
      message: 'Add a player to the tournament',
      validate: async name => name.length !== 0 || 'You must enter a value'
    }
  ])

  const players = (await findPlayers(playerName))
    .filter(player => !currentPlayers.find(cPlayer => cPlayer.backstabbrTag === player.backstabbrTag))
  if (players.length === 0) {
    console.log('Player is either already in the tournament, or doesn\'t exist')
    return getPlayers(currentPlayers)
  }

  let newPlayers
  if (players.length === 1) {
    newPlayers = [ ...currentPlayers, ...players ]
  } else {
    const { selectPlayer } = await inquirer.prompt([
      {
        name: 'selectPlayer',
        message: 'Select player from list',
        type: 'list',
        choices: players.map(player => ({
          name: `${player.realName} - (${player.backstabbrTag})`,
          value: player,
          short: player.backstabbrTag
        }))
      }
    ])

    newPlayers = [ ...currentPlayers, selectPlayer ]
  }
  if (newPlayers.length === 7 || !(await wantsMorePlayers())) return newPlayers 
  return getPlayers(newPlayers)
}

const getScoringConfig = async () => {
  const { scoringConfigName } = await inquirer.prompt([
    {
      name: 'scoringConfigName',
      message: 'What scoring config do you want to use, if blank, it will be the default config',
      filter: v => v.trim()
    }
  ])

  if (!scoringConfigName.length) {
    return null
  }
  const config = await findOneScoringConfig(scoringConfigName)
  if (!config) {
    return getScoringFormat()
  }

  return config
}

module.exports = async () => {
  
  const { name } = await inquirer.prompt([
    {
      name: 'name',
      message: 'What is the tournament name?',
      validate: async name => name.length !== 0 || 'You must enter a value'
    }
  ])


  const players = await getPlayers([])
  const { numberOfGames } = await inquirer.prompt([
    {
      name: 'numberOfGames',
      message: 'How many games should the tournament have? If 0 it will be indefinite',
      default: 0,
      validate: v => !isNaN(v),
      filter: v => parseInt(v)
    }
  ])

  const scoringConfig = await getScoringConfig()
  createTournament({
    name,
    players,
    scoringConfig,
    numberOfGames
  })
}