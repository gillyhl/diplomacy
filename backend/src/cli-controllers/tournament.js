const inquirer = require('inquirer')
const { findTournament, getTournamentStandings, addGameToTournament } = require('../services/tournament')
const { addHistoryToGame } = require('../services/game')
const moment = require('moment')
const _ = require('lodash')
const { TOURNAMENT_ACTIONS } = require('./constants')
const { scoreJson, processScoreForTournament } = require('../services/scoring')
const { table } = require('table')
const chalk = require('chalk')

const getSingleTournament = async name => {
  const tournaments = await findTournament(name)
  if (tournaments.length === 0) {
    console.log(`No tournament with name ${name} found.`)
  }

  if (tournaments.length === 1) {
    return tournaments[0]
  }

  const { tournament } = await inquirer.prompt([
    {
      name: 'tournament',
      message: 'Select tournament from list',
      type: 'list',
      choices: tournaments.map(t => ({
        name: `${t.name} - (${moment(t.date).format()})`,
        value: t
      }))
    }
  ])

  return tournament
}

const getTournamentAction = async () => {
  const { action } = await inquirer.prompt([
    {
      name: 'action',
      message: 'What would you like to do',
      type: 'list',
      choices: _.values(TOURNAMENT_ACTIONS)
    }
  ])

  return action
}

const scoreTournamentGame = async (name, file) => {
  const tournament = await getSingleTournament(name)
  const json = require(require('path').resolve(file))
  const score = scoreJson(json, tournament.scoringConfig)
  const results = await processScoreForTournament(score)
  addGameToTournament(tournament, { results })
}

const tournamentOverview = async name => {
  const tournament = await getSingleTournament(name)
  const action = await getTournamentAction()

  switch (action) {
    case TOURNAMENT_ACTIONS.ADD_GAME.value:
      console.log('add game')
      break
    case TOURNAMENT_ACTIONS.COMPLETE_TOURNAMENT.value:
      console.log('complete tournament')
      break
    case TOURNAMENT_ACTIONS.VIEW_PLAYERS.value:
      console.log('view players')
      break
    case TOURNAMENT_ACTIONS.VIEW_STANDINGS.value:
      const standings = await getTournamentStandings(tournament)
      console.log(`Standings after ${chalk.bold.green(tournament.games.length)} games ${tournament.numberOfGames ? `of ${chalk.bold.green(tournament.numberOfGames)}` : ''}`)
      const data = [
        ['#', 'PLAYER', 'PTS'],
        ...standings.map(result => {
          const { score, rank, player } = result
          
          const data = [ rank, `${player.realName} (${player.backstabbrTag})`, score ]
          return rank === 1 ? data.map(s => chalk.bold.bgGreen(s)) : data
        })
      ]
    
      const config = {}
    
      console.log(table(data, config))
      break
    default:
      break
  }
  
}

const addGameHistory = async (name, file) => {
  const tournament = await getSingleTournament(name)
  if (!tournament) return
  const questions = [
    {
      name: 'game',
      message: 'Which game would you like to set history for?',
      type: 'list',
      choices: tournament.games.map((game, i) => ({
        name: `Game ${i + 1}`,
        value: game._id
      }))
    }
  ]

  const { game } = await inquirer.prompt(questions)
  addHistoryToGame(game, file)
}

module.exports = {
  tournamentOverview,
  scoreTournamentGame,
  addGameHistory
}
