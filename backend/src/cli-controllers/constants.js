const TOURNAMENT_ACTIONS = {
  VIEW_PLAYERS: {
    name: 'View players',
    value: 'VIEW_PLAYERS'
  },
  VIEW_STANDINGS: {
    name: 'View current standings',
    value: 'VIEW_STANDINGS'
  },
  ADD_GAME: {
    name: 'Add game',
    value: 'ADD_GAME'
  },
  COMPLETE_TOURNAMENT: {
    name: 'Complete tournament',
    value: 'COMPLETE_TOURNAMENT'
  }
}

module.exports = {
  TOURNAMENT_ACTIONS
}