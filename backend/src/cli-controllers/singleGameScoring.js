const path = require('path')
const { COUNTRIES } = require('../constants')
const { scoreJson } = require('../services/scoring')
module.exports = file => {
  const _ = require('lodash')
  const { table } = require('table')

  const json = require(path.resolve(file))
  const resultsWithScore = scoreJson(json)

  const data = [
    ['#', 'COUNTRY', 'BALANCE', 'SCs', 'YEARS', 'POSITION', 'TOTAL'],
    ...resultsWithScore.map(result => {
      const { country, meta } = result
      const style = COUNTRIES[country].style
      return [meta.overallRank, style(country), meta.scoringBreakdown.balance, meta.scoringBreakdown.supplycentres, meta.scoringBreakdown.fullYears, meta.scoringBreakdown.position, meta.score]
    })
  ]

  const config = {}

  console.log(table(data, config))
}