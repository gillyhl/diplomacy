const router = require('express').Router()
const {
  createOauthClient,
  getAuthUrl,
  getTokens,
  getUserEmailFromToken,
  verifyToken,
  registerUser,
  getJwtForPlayer
} = require('../services/authentication')
const { findPlayerByEmail } = require('../services/players')
const {
  to,
  handleError,
  handleSuccess
} = require('../services/apiUtils')

module.exports = app => {
  app.use('/auth', router)
  router.get('/google-sign-in-url', async (req, res) => {
    const oauth2Client = createOauthClient()
    return handleSuccess(res, getAuthUrl(oauth2Client))
  })

  router.get('/callback', async (req, res) => {
    const {
      code
    } = req.query
    const oauth2Client = createOauthClient()
    const {
      data,
      error
    } = await to(getTokens(oauth2Client, code))
    if (error) return handleError(res, 'Unable to authenticate')
    const email = getUserEmailFromToken(data.tokens)
    const player = await findPlayerByEmail(email)
    return handleSuccess(res, {
      email,
      id_token: data.tokens.id_token,
      token: player && getJwtForPlayer(player)
    })
  })

  router.post('/register', async (req, res) => {
    const client = createOauthClient()
    const {
      data: token,
      error: verifyError
    } = await to(verifyToken(req.body.id_token, client))
    if (verifyError) return handleError(res, 'Unable to register user.')
    const {
      data: registeredPlayer,
      error: registerError
    } = await to(registerUser(token, req.body))
    if (registerError) return handleError(res, 'Unable to register user.')
    return handleSuccess(res, {
      token: getJwtForPlayer(registeredPlayer)
    })
  })
}