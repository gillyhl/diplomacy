const router = require('express').Router()
const { 
  createNationGuessGame,
  getNationGuessingGames,
  deleteNationGuessingGame,
  lockNationsGuessingGame,
  getNationGuessingGameById,
  queryPlayersInNationGuessingGame,
  addGuessToGame,
  decryptGuess } = require('../services/nationGuess')
const { to, handleError, handleSuccess } = require('../services/apiUtils')
module.exports = app => {
  app.use('/nation-guess', router)

  router.post('/create/:id', async (req, res) => {
    const { id } = req.params
    const { name } = req.body
    const { sub } =  req.user
    const { data, error } = await to(createNationGuessGame(id, name, sub))
    if (error) return handleError(res, 'Unable to create nation guessing game')
    return handleSuccess(res, data._id)
  })

  router.put('/:id', async (req, res) => {
    const { id } = req.params
    const { sub } =  req.user
    const { guess, password } = req.body
    const { data, error } = await to(addGuessToGame(id, sub, guess, password))
    if (error) return handleError(res, 'Unable to add guess')
    return handleSuccess(res, data)
  })
  
  router.put('/:id/decrypt', async (req, res) => {
    const { id } = req.params
    const { password } = req.body
    const { sub } =  req.user
    const { data, error } = await to(decryptGuess(id, sub, password))
    if (error) return handleError(res, 'Unable to decrypt guess')
    return handleSuccess(res, data)
  })

  router.patch('/:id/lock', async (req, res) => {
    const { id } = req.params
    const { sub } =  req.user
    const { error } = await to(lockNationsGuessingGame(id, sub))
    if (error) return handleError(res, 'Unable to lock nation guessing game')
    return handleSuccess(res, null)
  })

  router.get('/', async (req, res) => {
    const { sub } =  req.user
    const { data, error } = await to(getNationGuessingGames(sub))
    if (error) return handleError(res, 'Unable to get nation guessing games')
    return handleSuccess(res, data)
  })

  router.get('/:id', async (req, res) => {
    const { id } = req.params
    const { sub } =  req.user
    const { data, error } = await to(getNationGuessingGameById(id, sub))
    if (error) return handleError(res, 'Unable to get nation guessing games')
    if (data) return handleSuccess(res, data)
    return handleError(res, 'Game not found', 404)
  })

  router.get('/:id/query', async (req, res) => {
    const { id } = req.params
    const { q } = req.query
    const { data, error } = await to(queryPlayersInNationGuessingGame(id, q))
    if (error) return handleError(res, 'Unable to get nation guessing games')
    return handleSuccess(res, data)
  })

  router.delete('/:id', async (req, res) => {
    const { id } = req.params
    const { sub } =  req.user
    const { error } = await to(deleteNationGuessingGame(id, sub))
    if (error) return handleError(res, 'Unable to delete games')
    return handleSuccess(res, {}, 202)
  })
}