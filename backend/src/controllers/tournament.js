const router = require('express').Router()
const { listTournaments, getTournamentById, findTournamentsByName, getTournamentStats } = require('../services/tournament')
const { to, handleError, handleSuccess } = require('../services/apiUtils')

module.exports = app => {
  app.use('/tournament', router)

  router.get('/', async (req, res) => {
    const { sub } = req.user
    const { data, error } = await to(listTournaments(sub, {}))
    if (error) {
      return handleError(res, 'Unable to get tournaments')
    }

    return handleSuccess(res, data)
  })
  
  router.get('/query', async (req, res) => {
    const { q } = req.query
    const { data, error } = await to(findTournamentsByName(q))
    if (error) {
      return handleError(res, 'Unable to query tournaments')
    }

    return handleSuccess(res, data)
  })
  
  router.get('/:id', async (req, res) => {
    const { id } = req.params
    const { sub } = req.user
    const { data, error } = await to(getTournamentById(id, sub))
    if (error) return handleError(res, 'Unable to get tournament')
    if (data) return handleSuccess(res, data)
    return handleError(res, 'Tournament not found', 404)
  })
  
  router.get('/:id/stats', async (req, res) => {
    const { id } = req.params
    const { sub } = req.user
    const { data, error } = await to(getTournamentStats(id, sub))
    if (error) return handleError(res, 'Unable to get tournament stats')
    if (data) return handleSuccess(res, data)
  })

}