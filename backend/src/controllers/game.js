const router = require('express').Router()
const { to, handleError, handleSuccess } = require('../services/apiUtils')
const { getGameById } = require('../services/game')
module.exports = app => {
  app.use('/game', router)

  router.get('/:id', async (req, res) => {
    const { sub } = req.user
    const {data, error} = await to(getGameById(req.params.id, sub))
    if (error) return handleError(res, 'Unable to fetch game')
    if (data) return handleSuccess(res, data)
    return handleError(res, 'Unable to find game', 404)
  })
}