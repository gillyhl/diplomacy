const express = require('express')
const glob = require('glob')
const path = require('path')
const bodyParser = require('body-parser')

const port = process.env.PORT || 5000

const app = express()
const router = express.Router()

app.use(bodyParser.json())
app.use(/^\/api\/v1\/(?!auth)(.*)$/, require('./middleware/verifyJwt'))
app.use('/api/v1', router)
const matches = glob.sync(path.join(__dirname, './controllers/**/*.js'))
matches.forEach(match => {
  require(match)(router)
})

app.use(express.static(path.join(__dirname, '../../frontend/build')))
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '../../frontend/build/index.html'))
})

app.listen(port, () => console.log(`Diplomacy app, listening on port ${port}`))