const mongoose = require('mongoose')
const config = require('../config')
const MONGODB_URI = config.mongodb.host

const createConnection = () => mongoose.createConnection(MONGODB_URI, { 
  useNewUrlParser: true
})

module.exports = {
  createConnection
}