const { Schema } = require('mongoose')
const { playerSchema } = require('./Player')

const gameSchema = new Schema({
  results: [{
    player: playerSchema,
    rank: Number,
    score: Number,
    country: {
      type: String,
      enum: ['FRANCE', 'BRITAIN', 'GERMANY', 'TURKEY', 'RUSSIA', 'AUSTRIA', 'ITALY']
    },
    meta: Schema.Types.Mixed
  }],
  history: String
})

module.exports = {
  gameSchema,
  model: connection => connection.model('Game', gameSchema)
}