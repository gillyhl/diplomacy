const { Schema } = require('mongoose')

const playerSchema = new Schema({
  realName: String,
  backstabbrTag: String,
  email: String,
  refreshToken: String
})

module.exports = {
  playerSchema,
  model: connection => connection.model('Player', playerSchema)
}