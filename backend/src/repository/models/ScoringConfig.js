const { Schema } = require('mongoose')

const scoringConfigSchema = new Schema({
  name: String,
  pointsPerFullYear: Number,
  maxYearsBonus: Number,
  pointsPerSupplyCentre: Number,
  positionBonus: [Number]
})

module.exports = {
  scoringConfigSchema,
  model: connection => connection.model('ScoringConfig', scoringConfigSchema)
}