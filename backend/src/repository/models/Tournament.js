const { Schema } = require('mongoose')
const { playerSchema } = require('./Player')
const { scoringConfigSchema } = require('./ScoringConfig')
const { gameSchema } = require('./Game')

const tournamentSchema = new Schema({
  name: String,
  players: [ playerSchema ],
  scoringConfig: scoringConfigSchema,
  games: [ gameSchema ],
  numberOfGames: {
    type: Number,
    default: 0,
    required: true
  },
  date: { type: Date, default: Date.now },
  completed: { type: Boolean, default: false }
})

module.exports = {
  tournamentSchema,
  model: connection => connection.model('Tournament', tournamentSchema)
}