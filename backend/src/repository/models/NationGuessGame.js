const { Schema } = require('mongoose')
const { tournamentSchema } = require('./Tournament')
const { playerSchema } = require('./Player')

const playerGuessSchema = new Schema({
  player: playerSchema,
  encryptedGuess: {
    type: String,
    default: ''
  },
  decryptedGuess: {
    type: String,
    default: null
  },
  password: String
})

const nationGuessGameSchema = new Schema({
  tournament: tournamentSchema,
  gameCreator: playerSchema,
  name: String,
  guesses: {
    type: [playerGuessSchema],
    default: []
  },
  locked: {
    type: Boolean,
    default: false
  }
})

module.exports = {
  nationGuessGameSchema,
  model: connection => connection.model('NationGuessGame', nationGuessGameSchema)
}



