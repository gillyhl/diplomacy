const jwt = require('jsonwebtoken')
const { jwt: { secret }} = require('../config')
const { handleError } = require('../services/apiUtils')
module.exports = (req, res, next) => {
  const authHeader = req.headers.authorization
  if (authHeader && authHeader.split(' ')[0] === 'Bearer') { // Authorization: Bearer g1jipjgi1ifjioj
    // Handle token presented as a Bearer token in the Authorization header
    const result = jwt.verify(authHeader.split(' ')[1], secret)
    if (!result) return handleError(res, 'Unauthorized', 401)
    req.user = result
    next()
    return
  }
  return handleError(res, 'Unauthorized', 401)

  next()
}