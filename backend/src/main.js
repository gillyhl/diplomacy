#!/usr/bin/env node
const commandLineArgs = require('command-line-args')
const { tournamentOverview, scoreTournamentGame } = require('./cli-controllers/tournament')
const { addGameHistory } = require('./cli-controllers/tournament')

const optionDefinitions = [
  { name: 'single-game', alias: 'g', type: Boolean },
  { name: 'tournament-game', alias: 'G', type: String },
  { name: 'game-history', alias: 'h', type: String },
  { name: 'file', alias: 'f', type: String, defaultOption: true },
  { name: 'new-player', alias: 'P', type: Boolean },
  { name: 'new-tournament', alias: 'T', type: Boolean },
  { name: 'new-scoring', alias: 'S', type: Boolean },
  { name: 'tournament', alias: 't', type: String },
]

const options = commandLineArgs(optionDefinitions)

const singleGameScore = (file) => {
  require('./cli-controllers/singleGameScoring')(file)
}

const newPlayer = () => {
  require('./cli-controllers/newPlayer')()
}

const newTournament = () => {
  require('./cli-controllers/newTournament')()
}

const newScoring = () => {
  require('./cli-controllers/newScoring')()
}

const tournament = (name) => {
  tournamentOverview(name)
}

const tournamentGameScore = (name, file) => {
  scoreTournamentGame(name, file)
}

const addHistory = (name, file) => {
  addGameHistory(name, file)
}

if (options['single-game'] && options.file) {
  singleGameScore(options.file)
} else if (options['game-history']  && options.file) {
  addHistory(options['game-history'], options.file)
} else if (options['tournament-game'] && options.file) {
  tournamentGameScore(options['tournament-game'], options.file)
} else if (options['new-player']) {
  newPlayer()
} else if (options['new-tournament']) {
  newTournament()
} else if (options['new-scoring']) {
  newScoring()
} else if (options['tournament']) {
  tournament(options['tournament'])
}
