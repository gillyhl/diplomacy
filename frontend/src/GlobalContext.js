import React from 'react'

export default React.createContext({
  loading: false,
  setLoading: () => {},
  token: null,
  setToken: () => {},
  decodedUser: null,
  setDecodedUser: () => {}
})
