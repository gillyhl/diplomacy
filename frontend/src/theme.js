import { createMuiTheme } from '@material-ui/core/styles'
import green from '@material-ui/core/colors/green'
import red from '@material-ui/core/colors/red'
import yellow from '@material-ui/core/colors/yellow'
import lightBlue from '@material-ui/core/colors/lightBlue'
import blue from '@material-ui/core/colors/blue'
import pink from '@material-ui/core/colors/pink'
import grey from '@material-ui/core/colors/grey'

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    leader: yellow[900],
    currentUser: green[700],
    austria: red[700],
    italy: green[700],
    turkey: yellow[500],
    france: lightBlue[100],
    britain: blue[700],
    russia: pink[300],
    germany: grey[700],
  },
  typography: {
    useNextVariants: true,
  },
})

theme.overrides = {
  MuiTableRow: {
    root: {
      height: 30,
    },
  },
  MuiTableCell: {
    root: {
      [theme.breakpoints.down('md')]: {
        padding: 5,
      },
    },
  },
  MuiGrid: {
    item: {
      overflowY: 'auto',
    },
  },
}

export default theme
