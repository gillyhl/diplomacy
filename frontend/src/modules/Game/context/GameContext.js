import React from 'react'

const value = {
  game: null,
  setGame: () => {},
}

export default React.createContext(value)
