import createAxios from 'services/axios'
import genericAxiosCall from 'services/genericAxiosCall'

export const getGameById = id =>
  genericAxiosCall(createAxios().get(`/api/v1/game/${id}`))
