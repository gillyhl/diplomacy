import React from 'react'
import Typography from '@material-ui/core/Typography'
import Markdown from 'react-markdown'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  header: {
    marginTop: theme.spacing.unit,
  },
})

function WinnersHistory({ history, classes }) {
  return (
    <section>
      <Typography className={classes.header} variant="h4">
        Winner's History
      </Typography>
      <Markdown source={history} escapeHtml={false} />
    </section>
  )
}

export default withStyles(styles)(WinnersHistory)
