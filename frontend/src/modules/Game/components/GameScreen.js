import React, { useState, useEffect, useContext } from 'react'
import GameContext from '../context/GameContext'
import GlobalContext from 'GlobalContext'
import Grid from '@material-ui/core/Grid'
import ErrorMessage from 'components/ErrorMessage'
import GameInformation from './GameInformation'
import { getGameById } from '../service'

function GameScreen({ match }) {
  const { setLoading } = useContext(GlobalContext)
  const [game, setGame] = useState(null)
  const [errorMessage, setErrorMessage] = useState(false)

  const getGame = async () => {
    setLoading(true)
    const { result, error } = await getGameById(match.params.id)
    if (error) {
      console.error(error)
      setErrorMessage('Unable to fetch game')
    } else {
      setGame(result.data)
    }
    setLoading(false)
  }

  useEffect(() => {
    getGame()
  }, [])

  return (
    <GameContext.Provider value={{ game, setGame }}>
      <Grid container spacing={0}>
        {errorMessage && <ErrorMessage message={errorMessage} />}
        {game && <GameInformation />}
      </Grid>
    </GameContext.Provider>
  )
}

export default GameScreen
