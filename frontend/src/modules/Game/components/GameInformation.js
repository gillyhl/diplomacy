import React, { useContext } from 'react'
import GameContext from '../context/GameContext'
import GenericGrid from 'components/GenericGrid'
import PageTitle from 'components/PageTitle'
import GameScoringTable from 'components/GameScoringTable'
import WinnersHistory from './WinnersHistory'

function GameInformation() {
  const { game } = useContext(GameContext)
  return (
    <>
      <PageTitle>Game Details</PageTitle>
      <GenericGrid>
        <GameScoringTable game={game} isDetailed />
        {game.history && <WinnersHistory history={game.history} />}
      </GenericGrid>
    </>
  )
}

export default GameInformation
