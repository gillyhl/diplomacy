import createAxios from 'services/axios'
import genericAxiosCall from 'services/genericAxiosCall'

export const getTournaments = () =>
  genericAxiosCall(createAxios().get('/api/v1/tournament'))
