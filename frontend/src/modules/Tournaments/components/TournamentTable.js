import React, { useContext } from 'react'
import TournamentsContext from '../context/Tournaments'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import Button from '@material-ui/core/Button'
import { Link } from 'react-router-dom'

function TournamentTable() {
  const { tournaments } = useContext(TournamentsContext)
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Tournament Name</TableCell>
          <TableCell>Players</TableCell>
          <TableCell>View</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {tournaments.map(t => (
          <TableRow key={t._id}>
            <TableCell>{t.name}</TableCell>
            <TableCell>{t.players.map(p => p.realName).join(', ')}</TableCell>
            <TableCell>
              <Button
                variant="contained"
                color="primary"
                component={Link}
                to={`/tournament/${t._id}`}>
                View
              </Button>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  )
}

export default TournamentTable
