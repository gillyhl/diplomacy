import React, { useState, useEffect, useContext } from 'react'
import TournamentList from './TournamentList'
import TournamentContext from '../context/Tournaments'
import ErrorMessage from 'components/ErrorMessage'
import GlobalContext from 'GlobalContext'
import Grid from '@material-ui/core/Grid'
import { getTournaments } from '../service'

function TournamentsScreen() {
  const [tournaments, setTournaments] = useState(null)
  const { setLoading } = useContext(GlobalContext)
  const [errorMessage, setErrorMessage] = useState(null)

  useEffect(() => {
    fetchTournaments()
  }, [])

  const fetchTournaments = async () => {
    setLoading(true)
    const { result, error } = await getTournaments()
    if (error) {
      console.log(error)
      setErrorMessage('Unable to fetch tournaments')
    } else {
      setTournaments(result.data)
    }
    setLoading(false)
  }
  return (
    <TournamentContext.Provider value={{ tournaments, setTournaments }}>
      <Grid container spacing={0}>
        <ErrorMessage message={errorMessage} />
        {tournaments && <TournamentList />}
      </Grid>
    </TournamentContext.Provider>
  )
}

export default TournamentsScreen
