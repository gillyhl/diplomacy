import React from 'react'
import GenericGrid from 'components/GenericGrid'
import TournamentTable from './TournamentTable'

import PageTitle from 'components/PageTitle'

function TournamentList() {
  return (
    <>
      <PageTitle>Tournaments</PageTitle>
      <GenericGrid>
        <TournamentTable />
      </GenericGrid>
    </>
  )
}

export default TournamentList
