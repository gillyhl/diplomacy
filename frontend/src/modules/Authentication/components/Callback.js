import React, { useEffect } from 'react';
import queryString from 'query-string'
import useGenericLoadData from 'hooks/useGenericLoadData'
import { getToken } from '../service'
import ErrorMessage from 'components/ErrorMessage'
import { Redirect } from 'react-router-dom'
import GenericGrid from 'components/GenericGrid'
import Loading from 'components/Loading';

const Callback = ({ location }) => {
  const searchParams = queryString.parse(location.search)
  const [ loading, data, error ] = useGenericLoadData(() => getToken(searchParams))
  useEffect(() => {
    if (data && data.token) {
      localStorage.setItem('jwt', data.token)
    }
  }, [data])
  const successfulLoad = data && !loading

  return (
    <GenericGrid>
      { error && <ErrorMessage message={error.reason} /> } 
      { successfulLoad && !data.token && <Redirect to={`/register?${queryString.stringify({ email: data.email, id_token: data.id_token })}`} /> }
      { successfulLoad && data.token && <Redirect to='/' /> }
      <Loading show={loading} />
    </GenericGrid>
  );
};

export default Callback;