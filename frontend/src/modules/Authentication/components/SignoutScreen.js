import React, {useEffect} from 'react';
import GenericGrid from 'components/GenericGrid'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import clearUserSession from 'services/clearUserSession'

const StyledTypography = withStyles(theme => ({
  root: {
    margin: theme.spacing.unit
  }
}))(Typography)

const SignoutScreen = ({ history }) => {
  useEffect(() => {
    logout()
  })

  const logout = () => {
    setTimeout(() => {
      clearUserSession()
      history.push('/')
    }, 3000)
  }
  return (
    <GenericGrid>
      <StyledTypography>
        Ta-ta! See you later! Redirecting you to the homepage.
      </StyledTypography>
    </GenericGrid>
  );
};

export default SignoutScreen;