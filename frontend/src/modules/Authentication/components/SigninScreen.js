import { useContext, useEffect } from 'react'
import GlobalContext from 'GlobalContext'
import { getGoogleAuthUrl } from '../service'
import useGenericLoadData from 'hooks/useGenericLoadData'

const SigninScreen = () => {
  const [loading, data] = useGenericLoadData(() => getGoogleAuthUrl())
  const { setLoading } = useContext(GlobalContext)
  setLoading(loading)

  useEffect(() => {
    if (data) window.location = data
  }, [data])
  return null
}

export default SigninScreen
