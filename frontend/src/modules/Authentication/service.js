import createAxios from 'services/axios'
import genericAxiosCall from 'services/genericAxiosCall'
import queryString from 'query-string'
export const getGoogleAuthUrl = () =>
  genericAxiosCall(createAxios().get('/api/v1/auth/google-sign-in-url'))

export const getToken = (searchParams) => genericAxiosCall(createAxios().get(`/api/v1/auth/callback?${queryString.stringify(searchParams)}`))