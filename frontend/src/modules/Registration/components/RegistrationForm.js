import React, { useState } from 'react';
import queryString from 'query-string'
import TextField from '@material-ui/core/TextField'
import GenericGrid from 'components/GenericGrid'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import WithProgressSpinner from 'components/WithProgressSpinner'
import { isEmail } from 'validator'
import GenericFormErrorMessages from 'components/GenericFormErrorMessages'
import { registerUser } from '../service'
import ErrorMessage from 'components/ErrorMessage'
import useForm from 'hooks/useForm'

const styles = theme => ({
  form: {
    margin: theme.spacing.unit
  }
})

const StyledTextField = withStyles(theme => ({
  root: {
    margin: `${theme.spacing.unit}px 0`
  }
}))(TextField)

const RegistrationForm = ({ location, classes, history }) => {
  const { email, id_token } = queryString.parse(location.search)
  const [loading, setLoading] = useState(false)
  const [errorMessage, setErrorMessage] = useState(null)
  
  const formValues = {
    email: {
      value: email
    },
    backstabbrTag: {
      value: '',
      onChange: e => e.target.value
    }
  }

  const validations = {
    email: [
      {
        isValid: v => isEmail(v.email),
        message: 'Value must be a valid email'
      },
      {
        isValid: v => Boolean(v.email),
        message: 'Email is required'
      },
    ],
    backstabbrTag: [
      {
        isValid: v => v.backstabbrTag.match(/^[a-zA-Z0-9]+#\d+$/),
        message: 'Must be a valid backstabbr tag, like banana#12345'
      }
    ]
  }
  
  
  const handleSubmit = async (isValid, values) => {
    setErrorMessage(null)
    if (isValid) {
      setLoading(true)
      try {
        const { error, result } = await registerUser({
          ...values,
          id_token
        })
        if (error) {
          setErrorMessage(error.data && error.data.reason)
          return
        }
        localStorage.setItem('jwt', result.data.token)
        history.push('/')
        setLoading(false)
      } catch (e) {
        setLoading(false)
      }
      
    }
  }
  
  const [ formMeta, errors, submit, { isTouchedOrDirty }] = useForm(formValues, validations, handleSubmit)

  return (
    <GenericGrid>
      <form onSubmit={submit} className={classes.form}>
        {errorMessage && <ErrorMessage message={errorMessage} />}
        <StyledTextField
          fullWidth
          label="email"
          {...formMeta.email}
          disabled />
        <GenericFormErrorMessages errorMessages={errors.email} show={isTouchedOrDirty('email')} />
        <StyledTextField
          fullWidth
          label="backstabbr Tag"
          placeholder="Enter your backstabbr tag, this can be found on your profile"
          {...formMeta.backstabbrTag}
          />
        <GenericFormErrorMessages errorMessages={errors.backstabbrTag} show={isTouchedOrDirty('backstabbrTag')} />
        <WithProgressSpinner loading={loading}>
          <Button type='submit' variant='contained' color='primary'>
            Submit
          </Button>
        </WithProgressSpinner>
      </form>
    </GenericGrid>
  );
};

export default withStyles(styles)(RegistrationForm);