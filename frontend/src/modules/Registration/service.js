import createAxios from 'services/axios'
import genericAxiosCall from 'services/genericAxiosCall'

export const registerUser = formValues => genericAxiosCall(createAxios().post('/api/v1/auth/register', formValues))