import React from 'react';
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import CountryDisplay from 'components/CountryDisplay'
import _ from 'lodash'
import { withStyles } from '@material-ui/core/styles'
import classNames from 'classnames'

const styles = theme => ({
  leaderTableRow: {
    background: theme.palette.leader,
  }
})

const PointsPerCountry = ({ data, classes }) => {
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Pos</TableCell>
          <TableCell>Country</TableCell>
          <TableCell>Points</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {data.map(v => (
          <TableRow
            className={classNames({ [classes.leaderTableRow]: v.rank === 1 })}
            key={v.id}>
            <TableCell>{v.rank}</TableCell>
            <TableCell>
              <CountryDisplay country={v.country} />
            </TableCell>
            <TableCell>{v.score}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};

export default withStyles(styles)(PointsPerCountry);