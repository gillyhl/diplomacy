import React, { useState } from 'react';
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import Collapse from '@material-ui/core/Collapse'
import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import StarRate from '@material-ui/icons/StarRate'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { withStyles } from '@material-ui/core/styles'
import classNames from 'classnames'
import useGenericLoadData from 'hooks/useGenericLoadData'
import { getTournamentStatsById } from '../../service'
import PointsPerCountry from './PointsPerCountry'

const styles = {
  expandOpen: {
    transform: 'rotate(180deg)',
  }
}

const TournamentStats = ({ id, className, classes}) => {
  const [expanded, setExpanded] = useState(false)
  const [loading, data, error] = useGenericLoadData(() => getTournamentStatsById(id))
  return (
    <Card className={className}>
      <CardHeader 
        title='Tournament Stats'
        avatar={<Avatar><StarRate /></Avatar>} 
        action={<IconButton onClick={() => setExpanded(!expanded)}>
          <ExpandMoreIcon 
            className={classNames({ [classes.expandOpen]: expanded })} />
        </IconButton>} />
      <Collapse in={expanded} unmountOnExit>
        {data && <CardContent>
          <Typography variant='h6'>
            Points Per Country
          </Typography>
          <PointsPerCountry data={data.pointsPerCountry} />
        </CardContent>}
      </Collapse>
    </Card>
  );
};

export default withStyles(styles)(TournamentStats);