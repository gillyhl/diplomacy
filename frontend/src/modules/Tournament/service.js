import createAxios from 'services/axios'
import genericAxiosCall from 'services/genericAxiosCall'

export const getTournamentById = tournamentId =>
  genericAxiosCall(createAxios().get(`/api/v1/tournament/${tournamentId}`))

export const getTournamentStatsById = tournamentId =>
  genericAxiosCall(createAxios().get(`/api/v1/tournament/${tournamentId}/stats`))
