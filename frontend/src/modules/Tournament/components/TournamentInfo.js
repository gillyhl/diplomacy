import React, { useContext, useState } from 'react'
import Leaderboard from './Leaderboard'
import GameCard from './GameCard'
import TournamentContext from '../context/Tournament'
import Divider from '@material-ui/core/Divider'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import ChartDialog from './ChartDialog'
import _ from 'lodash'
import { Typography } from '@material-ui/core';
import TournamentStats from '../modules/Stats/TournamentStats'
const styles = theme => ({
  margin: {
    margin: '1rem 0',
  },
  chartButton: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  gamesCompleted: {
    marginTop: theme.spacing.unit,
  },
  statsHolder: {
    marginTop: theme.spacing.unit
  }
})

function TournamentInfo({ classes }) {
  const {
    tournament: { games, players, numberOfGames, _id }
  } = useContext(TournamentContext)
  const [openChartDialog, setOpenChartDialog] = useState(false)
  const gamesData = games.reduce(
    (acc, game) => {
      const latest = acc[acc.length - 1]
      return [
        ...acc,
        _.chain(game.results)
          .keyBy(r => r.player._id)
          .mapValues((r, k) => ({
            player: r.player,
            score: r.score + (latest ? latest[k].score : 0),
          }))
          .value(),
      ]
    },
    [
      _.chain(players)
        .keyBy(p => p._id)
        .mapValues(p => ({
          player: p,
          score: 0,
        }))
        .value(),
    ]
  )

  const chartFormattedGameData = gamesData.map((g, i) => ({
    ..._.chain(g)
      .mapKeys(v => v.player.backstabbrTag)
      .mapValues(v => v.score)
      .value(),
    name: `Game ${i}`,
  }))

  return (
    <section className={classes.margin}>
      <ChartDialog
        open={openChartDialog}
        onClose={() => setOpenChartDialog(false)}
        data={chartFormattedGameData}
        players={players}
      />
      <Button
        className={classes.chartButton}
        variant="contained"
        color="secondary"
        onClick={() => setOpenChartDialog(true)}>
        View Chart
      </Button>
      {numberOfGames && <Typography className={classes.gamesCompleted} variant='subtitle1'>{games.length === numberOfGames ? 'Tournament Complete' : <span>{`${games.length} games completed out of ${numberOfGames}`}</span>}</Typography>}
      <TournamentStats className={classes.statsHolder} id={_id} />
      <Leaderboard />
      <Divider className={classes.margin} />
      {games &&
        games.map((game, i) => (
          <GameCard game={game} key={game._id} gameNumber={i + 1} />
        ))}
    </section>
  )
}

export default withStyles(styles)(TournamentInfo)
