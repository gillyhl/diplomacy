import React, { useContext } from 'react'
import TournamentContext from '../context/Tournament'
import GlobalContext from 'GlobalContext'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import { withStyles } from '@material-ui/core/styles'
import classNames from 'classnames'

const styles = theme => ({
  leaderTableRow: {
    background: theme.palette.leader,
  },
  currentUser: {
    background: theme.palette.currentUser
  }
})

function Leaderboard({ classes }) {
  const { tournament } = useContext(TournamentContext)
  const { decodedUser } = useContext(GlobalContext)
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Pos</TableCell>
          <TableCell>Player</TableCell>
          <TableCell>Points</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {tournament.standings.map(t => (
          <TableRow
            key={t.id}
            className={classNames({ 
              [classes.currentUser]: t.rank !== 1 && decodedUser.sub === t.player._id, 
              [classes.leaderTableRow]: t.rank === 1
            })}>
            <TableCell>{t.rank}</TableCell>
            <TableCell>
              {t.player.realName} ({t.player.backstabbrTag})
            </TableCell>
            <TableCell>{t.score}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  )
}

export default withStyles(styles)(Leaderboard)
