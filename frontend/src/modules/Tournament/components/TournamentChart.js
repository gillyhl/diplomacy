import React from 'react'
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from 'recharts'
import { withTheme } from '@material-ui/core/styles'

function TournamentChart({ data, players, theme }) {
  const colors = [
    theme.palette.austria,
    theme.palette.france,
    theme.palette.britain,
    theme.palette.turkey,
    theme.palette.russia,
    'black',
    theme.palette.italy,
  ]
  return (
    <LineChart width={600} height={400} data={data}>
      <CartesianGrid strokeDasharray="3 3" tick={{ fill: 'white' }} />
      <XAxis dataKey="name" tick={{ fill: 'white' }} />
      <YAxis tick={{ fill: 'white' }} />
      <Tooltip tick={{ fill: 'black' }} />
      <Legend tick={{ fill: 'white' }} />
      {players.map((player, i) => (
        <Line
          type="monotone"
          key={player.backstabbrTag}
          dataKey={player.backstabbrTag}
          stroke={colors[i]}
        />
      ))}
    </LineChart>
  )
}

export default withTheme()(TournamentChart)
