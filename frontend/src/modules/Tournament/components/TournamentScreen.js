import React, { useState, useEffect, useContext } from 'react'
import Tournament from './Tournament'
import TournamentContext from '../context/Tournament'
import Grid from '@material-ui/core/Grid'
import GlobalContext from 'GlobalContext'
import ErrorMessage from 'components/ErrorMessage'
import { getTournamentById } from '../service'

function TournamentsScreen({ match }) {
  const [tournament, setTournament] = useState(null)
  const { setLoading } = useContext(GlobalContext)
  const [errorMessage, setErrorMessage] = useState(false)

  useEffect(() => {
    getTournament()
  }, [])

  const getTournament = async () => {
    setLoading(true)
    const { result, error } = await getTournamentById(match.params.id)
    if (error) {
      console.error(error)
      setErrorMessage('Unable to fetch tournament')
    } else {
      setTournament(result.data)
    }
    setLoading(false)
  }
  return (
    <TournamentContext.Provider value={{ tournament, setTournament }}>
      <Grid container spacing={0}>
        <ErrorMessage message={errorMessage} />
        {tournament && <Tournament />}
      </Grid>
    </TournamentContext.Provider>
  )
}

export default TournamentsScreen
