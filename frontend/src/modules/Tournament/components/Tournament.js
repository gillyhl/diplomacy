import React, { useContext } from 'react'
import GenericGrid from 'components/GenericGrid'
import TournamentContext from '../context/Tournament'
import TournamentInfo from './TournamentInfo'

import PageTitle from 'components/PageTitle'

function TournamentList() {
  const { tournament } = useContext(TournamentContext)
  return (
    <>
      <PageTitle>
        Tournament{tournament && tournament.name && ` - ${tournament.name}`}
      </PageTitle>
      <GenericGrid>{tournament && <TournamentInfo />}</GenericGrid>
    </>
  )
}

export default TournamentList
