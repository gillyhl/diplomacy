import React from 'react'
import Card from '@material-ui/core/Card'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import GameScoringTable from 'components/GameScoringTable'
import { Link } from 'react-router-dom'

const styles = {
  card: {
    marginTop: '1rem',
    padding: '0.5rem',
  },
  button: {
    float: 'right',
  },
}

function GameCard({ classes, game, gameNumber }) {
  return (
    <Card className={classes.card}>
      <Button
        variant="contained"
        color="secondary"
        className={classes.button}
        component={Link}
        to={`/game/${game._id}`}>
        View Details
      </Button>
      <Typography variant="h5">Game {gameNumber}</Typography>
      <GameScoringTable game={game} />
    </Card>
  )
}

export default withStyles(styles)(GameCard)
