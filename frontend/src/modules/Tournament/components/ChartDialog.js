import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import TournamentChart from './TournamentChart'
import { withStyles } from '@material-ui/core/styles'

const styles = {
  dialog: {
    overflow: 'hidden',
  },
}

function ChartDialog({ onClose, open, data, players, classes }) {
  return (
    <Dialog maxWidth={false} onClose={onClose} open={open}>
      <DialogTitle>Tournament Line Chart</DialogTitle>
      <DialogContent className={classes.dialog}>
        <TournamentChart data={data} players={players} />
      </DialogContent>
    </Dialog>
  )
}

export default withStyles(styles)(ChartDialog)
