import React from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'
import WithProgressSpinner from 'components/WithProgressSpinner'
import { Typography } from '@material-ui/core'
import useForm from 'hooks/useForm'
import GenericFormErrorMessages from 'components/GenericFormErrorMessages'

const StyledTextField = withStyles(theme => ({
  root: {
    marginBottom: theme.spacing.unit,
  },
}))(TextField)

const SubmitGuessForm = ({ onSubmit, submitting }) => {
  const formValues = {
    password1: {
      value: '',
      onChange: e => e.target.value
    },
    password2: {
      value: '',
      onChange: e => e.target.value
    },
    guess: {
      value: '',
      onChange: e => e.target.value
    }
  }

  const validations = {
    password1: [
      {
        isValid: values => Boolean(values.password1),
        message: 'Password is required',
      },
    ],
    password2: [
      {
        isValid: values => Boolean(values.password2),
        message: 'Confirm password is required',
      },
      {
        isValid: values => values.password1 === values.password2,
        message: 'Passwords must match',
      },
    ],
    guess: [
      {
        isValid: values => Boolean(values.guess),
        message: 'Guess is required',
      },
    ]
  }
  
  
  const handleSubmit = (isValid, formValues) => {
    if (isValid) {
      onSubmit(formValues)
    }
  }

  const [ formMeta, errors, submit, { isTouchedOrDirty } ] = useForm(formValues, validations, handleSubmit)

  return (
    <>
      <Typography variant="h6">Input Answer</Typography>
      <form onSubmit={submit}>
        <StyledTextField
          multiline
          fullWidth
          label="Guess"
          placeholder="Input guesses here, allows multiple lines"
          {...formMeta.guess}
        />
        <GenericFormErrorMessages errorMessages={errors.guess} show={isTouchedOrDirty('guess')} />
        <StyledTextField
          fullWidth
          label="Password"
          type="password"
          {...formMeta.password1}
        />
        <GenericFormErrorMessages errorMessages={errors.password1} show={isTouchedOrDirty('password1')} />
        <StyledTextField
          fullWidth
          label="Confirm Password"
          type="password"
          {...formMeta.password2}
        />
        <GenericFormErrorMessages errorMessages={errors.password2} show={isTouchedOrDirty('password2')} />
        <WithProgressSpinner loading={submitting}>
          <Button type="submit" variant="contained" color="primary">
            Submit
          </Button>
        </WithProgressSpinner>
      </form>
    </>
  )
}

export default SubmitGuessForm
