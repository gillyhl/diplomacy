import React, { useContext, useState } from 'react'
import PageTitle from 'components/PageTitle'
import { getGuessTheNationGameById, addGuess, decryptAnswer } from '../service'
import useGenericLoadData from 'hooks/useGenericLoadData'
import GlobalContext from 'GlobalContext'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import GuessesMade from './GuessesMade'
import SubmitGuessForm from './SubmitGuessForm'
import DecryptAnswerForm from './DecryptAnswerForm'
import ErrorMessage from 'components/ErrorMessage'

const GridItem = withStyles(theme => ({
  item: {
    padding: theme.spacing.unit,
  },
}))(Grid)

const GuessTheNationGameScreen = ({ match, classes }) => {
  const { setLoading } = useContext(GlobalContext)
  const [submitting, setSubmitting] = useState(false)
  const [submitResult, setSubmitResult] = useState(null)
  const [errorMessage, setErrorMessage] = useState(null)
  const [loading, game] = useGenericLoadData(
    () => getGuessTheNationGameById(match.params.id),
    submitResult
  )
  setLoading(loading)

  const handleSubmitGuess = async values => {
    setSubmitting(true)
    setErrorMessage(false)
    try {
      const { result } = await addGuess(match.params.id, {
        ...values,
        password: values.password1,
      })

      setSubmitResult(result)
    } finally {
      setSubmitting(false)
    }
  }

  const handleSubmitDecrypt = async values => {
    setSubmitting(true)
    setErrorMessage(false)
    try {
      const { result, error } = await decryptAnswer(match.params.id, values)
      if (error) {
        setErrorMessage(error.data && error.data.reason)
      } else {
        setSubmitResult(result)
      }
    } finally {
      setSubmitting(false)
    }
  }

  const guesses = (game && game.guesses) || []

  return (
    <section>
      <PageTitle>
        {game ? `${game.tournament.name} - ${game.name}` : 'Guess The Nation Game'}
      </PageTitle>
      {errorMessage && <ErrorMessage message={errorMessage} />}
      {game && (
        <Grid container spacing={0}>
          <GridItem item md={6} xs={12}>
            <Typography variant="h6">Submitted Answers</Typography>
            <GuessesMade guesses={guesses} />
          </GridItem>
          <GridItem item md={6} xs={12}>
            {game.locked ? (
              <DecryptAnswerForm
                onSubmit={handleSubmitDecrypt}
                gameId={match.params.id}
                submitting={submitting}
              />
            ) : (
              <SubmitGuessForm
                onSubmit={handleSubmitGuess}
                gameId={match.params.id}
                submitting={submitting}
              />
            )}
          </GridItem>
        </Grid>
      )}
    </section>
  )
}

export default GuessTheNationGameScreen
