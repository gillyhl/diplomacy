import React, { useState } from 'react'
import PageTitle from 'components/PageTitle'
import Done from '@material-ui/icons/Done'
import GenericAutocomplete from 'components/GenericAutocomplete'
import GenericGrid from 'components/GenericGrid'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'
import { createGuessTheNationGame } from '../service'
import GuessTheNationGames from './GuessTheNationGames'
import queryTournament from 'services/queryTournaments'
import TextField, {} from '@material-ui/core/TextField'

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
})

const GuessTheNationIndexScreen = ({ classes }) => {
  const [selectedTournament, setSelectedTournament] = useState(null)
  const [successfulCreations, setSuccessfulCreations] = useState(0)
  const [gameName, setGameName] = useState('')
  const handleTournamentAutoCompleteChange = selection =>
    setSelectedTournament(selection)

  const createGame = async () => {
    const { error } = await createGuessTheNationGame(selectedTournament._id, gameName)
    if (error) console.log(error)
    else {
      setSuccessfulCreations(successfulCreations + 1)
    }
  }

  const fetchTournaments = async value => {
    const result = await queryTournament(value)
    return result.data
  }

  return (
    <section>
      <PageTitle>
        <Done /> Guess The Nation
      </PageTitle>
      <GenericGrid>
        <GenericAutocomplete
          onChange={handleTournamentAutoCompleteChange}
          fetchSuggestions={fetchTournaments}
          itemToString={item => (item ? item.name : '')}
          label="Tournament Name"
          placeholder="Enter tournament name"
        />
        <TextField 
          value={gameName}
          onChange={e => setGameName(e.target.value)}
          fullWidth
          placeholder='Enter name for game'
          label='Game Name'
        />
        <Button
          disabled={!(selectedTournament && gameName)}
          className={classes.button}
          variant="contained"
          color="primary"
          onClick={createGame}>
          Create Game
        </Button>
        <GuessTheNationGames successfulCreations={successfulCreations} />
      </GenericGrid>
    </section>
  )
}

export default withStyles(styles)(GuessTheNationIndexScreen)
