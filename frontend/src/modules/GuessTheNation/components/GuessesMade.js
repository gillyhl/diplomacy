import React from 'react'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import { withStyles } from '@material-ui/core/styles'
import LockOpen from '@material-ui/icons/LockOpen'

const StyledPaper = withStyles(theme => ({
  root: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
  },
}))(Paper)

const styles = theme => ({
  wrapper: {
    position: 'relative',
  },
  icon: {
    position: 'absolute',
    top: 0,
    right: 0,
    margin: theme.spacing.unit,
  },
})

const GuessesMade = ({ guesses, classes }) => {
  return (
    <div>
      {guesses.map(guess => (
        <StyledPaper key={guess._id} className={classes.wrapper}>
          {guess.decryptedGuess && <LockOpen className={classes.icon} />}
          <Typography>
            {`${guess.player.realName} - ${guess.player.backstabbrTag}`}
          </Typography>
          {guess.decryptedGuess && <hr />}
          {guess.decryptedGuess &&
            guess.decryptedGuess
              .split('\n')
              .map((str, i) => <Typography key={i}>{str}</Typography>)}
        </StyledPaper>
      ))}
    </div>
  )
}

export default withStyles(styles)(GuessesMade)
