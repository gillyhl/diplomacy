import React from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'
import WithProgressSpinner from 'components/WithProgressSpinner'
import { Typography } from '@material-ui/core'
import red from '@material-ui/core/colors/red'
import useForm from 'hooks/useForm'
import GenericFormErrorMessages from 'components/GenericFormErrorMessages'

const StyledTextField = withStyles(theme => ({
  root: {
    marginBottom: theme.spacing.unit,
  },
}))(TextField)

const styles = {
  errorMessages: {
    color: red[500],
  },
}

const DecryptAnswerForm = ({ onSubmit, submitting }) => {
  const formValues = {
    password: {
      value: '',
      onChange: e => e.target.value
    }
  }

  const formValidators = {
    password: [
      {
        isValid: v => Boolean(v.password),
        message: 'Password is required'
      }
    ]
  }

  const handleSubmit = (isValid, formValues) => {
    if (isValid) {
      onSubmit(formValues)
    }
  }
  
  const [ formData, errors, formSubmit, { isTouchedOrDirty } ] = useForm(formValues, formValidators, handleSubmit)


  return (
    <>
      <Typography variant="h6">Decrypt Answer</Typography>
      <form onSubmit={formSubmit}>
        <StyledTextField
          fullWidth
          label="Password"
          type="password"
          {...formData.password}
        />
        <GenericFormErrorMessages errorMessages={errors.password} show={isTouchedOrDirty('password')} />
        <WithProgressSpinner loading={submitting}>
          <Button type="submit" variant="contained" color="primary">
            Submit
          </Button>
        </WithProgressSpinner>
      </form>
    </>
  )
}

export default withStyles(styles)(DecryptAnswerForm)
