import React, { useState, useEffect, useContext } from 'react'
import Typography from '@material-ui/core/Typography'
import {
  getGuessTheNationGames,
  lockGuessTheNationGame
} from '../service'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import Button from '@material-ui/core/Button'
import Lock from '@material-ui/icons/Lock'
import { withStyles } from '@material-ui/core/styles'
import { Link } from 'react-router-dom'
import GlobalContext from 'GlobalContext'

const styles = theme => ({
  button: {
    margin: theme.spacing.unit / 2,
  },
  gameIdTableCell: {
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
})

const StyledLock = withStyles({
  root: {
    display: 'inline-flex',
    verticalAlign: 'middle',
  },
})(Lock)

const GuessTheNationGames = ({ successfulCreations = 0, classes }) => {
  const [games, setGames] = useState([])
  useEffect(() => {
    fetchGuessTheNationGames()
  }, [successfulCreations])
  const { decodedUser } = useContext(GlobalContext)
  
  const fetchGuessTheNationGames = async () => {
    const { result, error } = await getGuessTheNationGames()
    if (error) console.log(error)
    else setGames(result.data)
  }
  
  const lockGame = async id => {
    const { error } = await lockGuessTheNationGame(id)
    if (error) console.log(error)
    else fetchGuessTheNationGames()
  }
  
  return (
    <section>
      <Typography variant="h5">Games</Typography>

      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Tournament Name</TableCell>
            <TableCell>Game Name</TableCell>
            <TableCell className={classes.gameIdTableCell}>Game ID</TableCell>
            <TableCell>Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {games.map(game => (
            <TableRow key={game._id}>
              <TableCell>{game.tournament.name}</TableCell>
              <TableCell>
                {game.name || 'Guessing Game'}
              </TableCell>
              <TableCell className={classes.gameIdTableCell}>
                {game._id}
              </TableCell>
              <TableCell>
                {game.locked ? (
                  <StyledLock />
                ) : 
                  game.gameCreator && game.gameCreator._id === decodedUser.sub && <Button
                    className={classes.button}
                    variant="contained"
                    color="primary"
                    onClick={() => lockGame(game._id)}
                    title="Lock game">
                    Lock
                  </Button>
                }{' '}
                <Button
                  className={classes.button}
                  variant="contained"
                  color="secondary"
                  component={Link}
                  to={`/guess-the-nation/${game._id}`}>
                  View
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </section>
  )
}

export default withStyles(styles)(GuessTheNationGames)
