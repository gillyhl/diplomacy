import createAxios from 'services/axios'
import genericAxiosCall from 'services/genericAxiosCall'
import queryString from 'query-string'

export const createGuessTheNationGame = (tournamentId, gameName) =>
  genericAxiosCall(createAxios().post(`/api/v1/nation-guess/create/${tournamentId}`, { name: gameName }))
export const getGuessTheNationGames = () =>
  genericAxiosCall(createAxios().get('/api/v1/nation-guess'))
export const getGuessTheNationGameById = id =>
  genericAxiosCall(createAxios().get(`/api/v1/nation-guess/${id}`))
export const queryUsersForNationGame = (id, query) =>
  genericAxiosCall(
    createAxios().get(
      `/api/v1/nation-guess/${id}/query?${queryString.stringify({ q: query })}`
    )
  )
export const clearGuessTheNationGames = () =>
  genericAxiosCall(createAxios().delete('/api/v1/nation-guess'))
export const lockGuessTheNationGame = id =>
  genericAxiosCall(createAxios().patch(`/api/v1/nation-guess/${id}/lock`))
export const addGuess = (id, { player, guess, password }) =>
  genericAxiosCall(
    createAxios().put(`/api/v1/nation-guess/${id}`, { player, guess, password })
  )
export const decryptAnswer = (id, { player, password }) =>
  genericAxiosCall(
    createAxios().put(`/api/v1/nation-guess/${id}/decrypt`, { player, password })
  )
