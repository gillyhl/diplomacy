import React from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import CardActions from '@material-ui/core/CardActions'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import GenericGrid from 'components/GenericGrid'
import ExitToApp from '@material-ui/icons/ExitToApp'
import { Link } from 'react-router-dom'

const styles = theme => ({
  card: {
    maxWidth: 400,
    margin: `${theme.spacing.unit * 2}px auto` 
  },
  buttonIcon: {
    marginRight: theme.spacing.unit
  },
  cardMedia: {
    height: 0,
    padding: '30%'
  }
})

const StyledTypography = withStyles(theme => ({
  root: {
    margin: theme.spacing.unit
  }
}))(Typography)

const HomeScreen = ({ classes }) => {
  return (
    <section>
      <GenericGrid>
        <Card className={classes.card}>
          <CardHeader title='Diplomacy' subheader='The Diplomacy Tournament Tracking App' />
          <CardMedia
            className={classes.cardMedia}
            image={`${process.env.PUBLIC_URL}/new-map.gif`}
            title='Diplomacy Map' />
          <CardContent>
            <StyledTypography variant='body1'>
              Create, modify and view your own personal Diplomacy tournaments. Scoring methods can be tailor made to suit your needs.
            </StyledTypography>
            <StyledTypography variant='body1'>
              Sign in to join in on the fun.
            </StyledTypography>
          </CardContent>
          <CardActions>
            <Button fullWidth color='secondary' variant='contained' component={Link} to='/sign-in'>
              <ExitToApp className={classes.buttonIcon} /> Sign In
            </Button>
          </CardActions>
        </Card>
      </GenericGrid>
    </section>
  )
}

export default withStyles(styles)(HomeScreen)
