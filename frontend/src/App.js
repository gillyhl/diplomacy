import React, { useState, useEffect } from 'react'
import Layout from './components/Layout'
import Tournaments from './modules/Tournaments/components/TournamentsScreen'
import Tournament from './modules/Tournament/components/TournamentScreen'
import Game from './modules/Game/components/GameScreen'
import GuessTheNationGameScreen from './modules/GuessTheNation/components/GuessTheNationGameScreen'
import GuessTheNationIndexScreen from './modules/GuessTheNation/components/GuessTheNationIndexScreen'
import Callback from './modules/Authentication/components/Callback'
import SigninScreen from './modules/Authentication/components/SigninScreen'
import SignoutScreen from './modules/Authentication/components/SignoutScreen'
import RegistrationForm from './modules/Registration/components/RegistrationForm'
import HomeScreen from './modules/Home/components/HomeScreen'
import PageNotFound from 'components/PageNotFound'
import GlobalContext from './GlobalContext'
import { withStyles } from '@material-ui/core/styles'
import jwt_decode from 'jwt-decode'
import { Route, Redirect, Switch, withRouter } from 'react-router-dom'
import queryString from 'query-string'
import getUserSession from 'services/getUserSession'
import checkUserSession from 'services/checkUserSession'
import clearUserSession from 'services/clearUserSession'
import f from 'services/axios'

const App = ({ location, history }) => {
  const [loading, setLoading] = useState(false)
  const [token, setToken] = useState(null)
  const [decodedUser, setDecodedUser] = useState(null)
  const contextValue = {
    loading,
    setLoading,
    token,
    setToken,
    decodedUser,
    setDecodedUser
  }

  const jwt = getUserSession()
  
  useEffect(() => {
    f()
    setToken(jwt)
    setDecodedUser(jwt ? jwt_decode(jwt) : null)
  }, [jwt])

  useEffect(() => {
    if (jwt) {
      const userIsValid = checkUserSession()

      if (!userIsValid) {
        clearUserSession()
        history.push(`/?${queryString.stringify({ next: window.location })}`)
      } 
    }
  }, [location])

  return (
    <GlobalContext.Provider value={contextValue}>
      {jwt && <Layout>
        <Switch>
          <Route exact path="/tournaments" component={Tournaments} />
          <Route exact path="/" component={Tournaments} />
          <Route path="/tournament/:id" component={Tournament} />
          <Route path="/game/:id" component={Game} />
          <Route
            exact
            path="/guess-the-nation"
            component={GuessTheNationIndexScreen}
          />
          <Route
            exact
            path="/guess-the-nation/:id"
            component={GuessTheNationGameScreen}
          />
          <Route exact path="/sign-out" component={SignoutScreen} />
          <Route component={PageNotFound} />
        </Switch>
      </Layout>}
      {!jwt && <Switch>
        <Route exact path="/auth/callback" component={Callback} />
        <Route exact path="/sign-in" component={SigninScreen} />
        <Route path='/register' component={RegistrationForm} />
        <Route exact path='/' component={HomeScreen} />
        <Redirect to={`/?${queryString.stringify({ next: window.location })}`} />
      </Switch>
      }
    </GlobalContext.Provider>
  )
}

export default [withStyles(theme => ({
  '@global': {
    body: {
      ...theme.typography.body1,
    },
  },
})), withRouter].reduce((acc, cur) => cur(acc), App)
