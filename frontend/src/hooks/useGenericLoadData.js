import { useState, useEffect } from 'react'

/**
 * Service method should be an async function that returns an object with keys:
 *  result: the result value
 *  error: the error response value
 */
export default (serviceMethod, ...watchers) => {
  const [data, setData] = useState(null)
  const [error, setError] = useState(null)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    fetchData()
  }, [...watchers])

  const fetchData = async () => {
    setLoading(true)
    try {
      const { result, error } = await serviceMethod()
      if (result) setData(result.data)
      if (error) setError(error.data)
    } catch (e) {
    } finally {
      setLoading(false)
    }
  }

  return [loading, data, error]
}
