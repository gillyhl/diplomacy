import React from 'react';
import Typography from '@material-ui/core/Typography'
import red from '@material-ui/core/colors/red'

const GenericFormErrorMessages = ({ errorMessages, show }) => {
  return (
    <>
     {show && errorMessages.map(message => (
        <Typography style={{ color: red[500] }} key={message}>
          {message}
        </Typography>
      ))} 
    </>
  );
};

export default GenericFormErrorMessages