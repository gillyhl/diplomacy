import React, { useState } from 'react'
import Downshift from 'downshift'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
import MenuItem from '@material-ui/core/MenuItem'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  paper: {
    marginTop: theme.spacing.unit,
  },
})

const Display = withStyles(styles)(
  ({
    getInputProps,
    getMenuProps,
    getItemProps,
    classes,
    isOpen,
    highlightedIndex,
    label,
    placeholder,
    fetchSuggestions,
    itemToString,
  }) => {
    const [suggestions, setSuggestions] = useState([])
    const onChange = async e => {
      try {
        const result = await fetchSuggestions(e.target.value)
        setSuggestions(result)
      } catch (e) {
        console.error(e)
      }
    }

    return (
      <>
        <TextField
          {...getInputProps({
            placeholder,
            label,
            fullWidth: true,
            onChange,
          })}
        />
        <div {...getMenuProps()}>
          {isOpen && (
            <Paper className={classes.paper}>
              {suggestions.map((item, index) => (
                <MenuItem
                  selected={highlightedIndex === index}
                  {...getItemProps({ item, index, key: item._id })}>
                  {itemToString(item)}
                </MenuItem>
              ))}
            </Paper>
          )}
        </div>
      </>
    )
  }
)

const GenericAutocomplete = ({
  classes,
  onChange,
  itemToString,
  label,
  placeholder,
  fetchSuggestions,
}) => {
  return (
    <Downshift
      classes={classes}
      children={props => (
        <div>
          <Display
            {...props}
            label={label}
            placeholder={placeholder}
            fetchSuggestions={fetchSuggestions}
            itemToString={itemToString}
          />
        </div>
      )}
      itemToString={itemToString}
      onChange={onChange}
    />
  )
}

export default withStyles(styles)(GenericAutocomplete)
