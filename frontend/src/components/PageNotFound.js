import React from 'react'
import Typography from '@material-ui/core/Typography'
import GenericGrid from 'components/GenericGrid'

const PageNotFound = () => {
  return (
    <section>
      <GenericGrid>
        <Typography variant='body1'>
          Page Not Found
        </Typography>
      </GenericGrid>
    </section>
  )
}

export default PageNotFound
