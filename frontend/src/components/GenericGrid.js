import React from 'react'
import Grid from '@material-ui/core/Grid'

const GenericGrid = ({ children }) => {
  return (
    <Grid container spacing={0}>
      <Grid item xs={1} sm={2} />
      <Grid item xs={10} sm={8}>
        {children}
      </Grid>
      <Grid item xs={1} sm={2} />
    </Grid>
  )
}

export default GenericGrid
