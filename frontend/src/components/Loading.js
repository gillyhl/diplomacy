import React from 'react'
import { HashLoader } from 'react-spinners'
import Grid from '@material-ui/core/Grid'
import { withTheme } from '@material-ui/core/styles'
import { css } from '@emotion/core'

const override = css`
  margin: 2rem auto;
`

function Loading({ show, theme }) {
  return (
    <Grid container spacing={0}>
      <Grid item xs={12}>
        {show && (
          <HashLoader color={theme.palette.secondary.main} css={override} />
        )}
      </Grid>
    </Grid>
  )
}

export default withTheme()(Loading)
