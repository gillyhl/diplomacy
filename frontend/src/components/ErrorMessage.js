import React from 'react'
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
  card: {
    background: theme.palette.error.dark,
    padding: '1rem',
    margin: '1rem',
  },
})

function ErrorMessage({ message, classes }) {
  return (
    <>
      {message && (
        <Grid item xs={12}>
          <Card className={classes.card}>
            <Typography>{message}</Typography>
          </Card>
        </Grid>
      )}
    </>
  )
}

export default withStyles(styles)(ErrorMessage)
