import React, { useContext } from 'react'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import { withStyles } from '@material-ui/core/styles'
import classNames from 'classnames'
import CountryDisplay from './CountryDisplay'
import GlobalContext from 'GlobalContext'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';

const styles = theme => ({
  leaderTableRow: {
    background: theme.palette.leader,
  },
  currentUser: {
    background: theme.palette.currentUser
  },
  backstabbrTag: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
})

function GameScoringTable({ classes, game, isDetailed, width }) {
  const { decodedUser } = useContext(GlobalContext)
  const tablePadding = isWidthUp('lg', width) ? 'dense' : 'default'
  return (
    <section style={{ overflowX: 'auto', width: '100%'}}>
      <Table padding={tablePadding}>
        <TableHead>
          <TableRow>
            <TableCell>Pos</TableCell>
            <TableCell>Player</TableCell>
            <TableCell>Country</TableCell>
            {
              isDetailed && <>
                <TableCell>Balance</TableCell>
                <TableCell>Full Years</TableCell>
                <TableCell>Position</TableCell>
                <TableCell>SCs</TableCell>
              </>
            }
            <TableCell>Points</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {game.results.map(t => (
            <TableRow
              key={t._id}
              className={classNames({ 
                [classes.currentUser]: t.rank !== 1 && decodedUser.sub === t.player._id, 
                [classes.leaderTableRow]: t.rank === 1
              })}>
              <TableCell>{t.rank}</TableCell>
              <TableCell>
                {t.player.realName}{' '}
                <span className={classes.backstabbrTag}>
                  ({t.player.backstabbrTag})
                </span>
              </TableCell>
              <TableCell>
                <CountryDisplay country={t.country} />
              </TableCell>
              {
                isDetailed && <>
                  <TableCell>{t.meta.balance}</TableCell>
                  <TableCell>{t.meta.fullYears}</TableCell>
                  <TableCell>{t.meta.position}</TableCell>
                  <TableCell>{t.meta.supplycentres}</TableCell>
                </>
              }
              <TableCell>{t.score}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </section>
  )
}

export default withWidth()(withStyles(styles)(GameScoringTable))
