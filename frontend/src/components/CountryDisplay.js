import React from 'react'
import _ from 'lodash'
import { withStyles, withTheme } from '@material-ui/core/styles'

const styles = {
  countryIcon: {
    width: '1rem',
    display: 'inline-block',
    height: '1rem',
    border: '1px solid black',
    marginRight: '0.5rem',
  },
}

function CountryDisplay({ country, classes, theme }) {
  const lowerCountry = _.toLower(country)
  const style = {
    background: theme.palette[lowerCountry],
  }
  return (
    <>
      <span className={classes.countryIcon} style={style} />
      {_.startCase(lowerCountry)}
    </>
  )
}

export default withTheme()(withStyles(styles)(CountryDisplay))
