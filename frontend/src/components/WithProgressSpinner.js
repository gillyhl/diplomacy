import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'

const styles = theme => ({
  wrapper: {
    position: 'relative',
  },
  spinner: {
    top: '50%',
    marginTop: '-12px',
    position: 'absolute',
    marginLeft: theme.spacing.unit,
  },
})

const WithProgressSpinner = ({ loading, classes, children }) => {
  return (
    <div className={classes.wrapper}>
      {children}
      {loading && (
        <CircularProgress
          className={classes.spinner}
          color="primary"
          size={24}
        />
      )}
    </div>
  )
}

export default withStyles(styles)(WithProgressSpinner)
