import React from 'react'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'

const styles = {
  typographyRoot: {
    padding: '1rem',
  },
}

function PageTitle({ children, classes }) {
  return (
    <Grid container spacing={0}>
      <Grid item xs={12}>
        <Typography variant="h4" className={classes.typographyRoot}>
          {children}
        </Typography>
      </Grid>
    </Grid>
  )
}

export default withStyles(styles)(PageTitle)
