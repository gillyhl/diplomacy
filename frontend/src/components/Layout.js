import React, { useContext, useState } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import GlobalContext from '../GlobalContext'
import { withStyles } from '@material-ui/core/styles'
import MenuIcon from '@material-ui/icons/Menu'
import IconButton from '@material-ui/core/IconButton'
import { Link } from 'react-router-dom'
import Loading from './Loading'
import Menu from './Menu'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  noUnderline: {
    textDecoration: 'none',
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  menuIcon: {
    margin: `0 ${theme.spacing.unit}`,
  },
})

function Layout({ children, classes }) {
  const { loading, decodedUser } = useContext(GlobalContext)
  const [open, setOpen] = useState(false)
  const toggleOpen = () => setOpen(!open)
  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <IconButton className={classes.menuIcon} onClick={toggleOpen}>
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            color="inherit"
            className={`${classes.grow} ${classes.noUnderline}`}
            component={Link}
            to="/">
            Diplomacy
          </Typography>
          {decodedUser && <Typography variant="caption" style={{ cursor: 'pointer', marginRight: '1rem' }}>
            {decodedUser.realName}
          </Typography>}
          <Typography variant="caption">
            {process.env.REACT_APP_DIPLOMACY_VERSION}
          </Typography>
        </Toolbar>
        <Menu open={open} toggleMenu={toggleOpen} />
      </AppBar>
      {children}
      <Loading show={loading} />
    </>
  )
}

export default withStyles(styles)(Layout)
