import React, { useState } from 'react'
import Downshift from 'downshift'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
import MenuItem from '@material-ui/core/MenuItem'
import { withStyles } from '@material-ui/core/styles'
import queryTournament from '../services/queryTournaments'

const styles = theme => ({
  paper: {
    marginTop: theme.spacing.unit,
  },
})

const Display = withStyles(styles)(
  ({
    getInputProps,
    getMenuProps,
    getItemProps,
    classes,
    isOpen,
    highlightedIndex,
  }) => {
    const [tournaments, setTournaments] = useState([])
    const fetchTournaments = async e => {
      try {
        const result = await queryTournament(e.target.value)
        setTournaments(result.data)
      } catch (e) {
        console.error(e)
      }
    }
    return (
      <>
        <TextField
          {...getInputProps({
            placeholder: 'Enter tournament name',
            label: 'Tournament Name',
            fullWidth: true,
            onChange: fetchTournaments,
          })}
        />
        <div {...getMenuProps()}>
          {isOpen && (
            <Paper className={classes.paper}>
              {tournaments.map((item, index) => (
                <MenuItem
                  selected={highlightedIndex === index}
                  {...getItemProps({ item, index, key: item._id })}>
                  {item.name}
                </MenuItem>
              ))}
            </Paper>
          )}
        </div>
      </>
    )
  }
)

const TournamentAutocomplete = ({ classes, onChange }) => {
  return (
    <Downshift
      classes={classes}
      children={props => (
        <div>
          <Display {...props} />
        </div>
      )}
      itemToString={item => (item ? item.name : '')}
      onChange={onChange}
    />
  )
}

export default withStyles(styles)(TournamentAutocomplete)
