import React, { useContext } from 'react'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'
import Done from '@material-ui/icons/Done'
import Gamepad from '@material-ui/icons/Gamepad'
import ExitToApp from '@material-ui/icons/ExitToApp'
import DirectionsRun from '@material-ui/icons/DirectionsRun'
import { Link } from 'react-router-dom'
import GlobalContext from 'GlobalContext'

const Menu = ({ open, toggleMenu }) => {
  const { decodedUser } = useContext(GlobalContext)
  return (
    <Drawer open={open} onClose={toggleMenu}>
      <List onClick={toggleMenu}>
        <ListItem button component={Link} to="/tournaments">
          <ListItemIcon>
            <Gamepad />
          </ListItemIcon>
          <ListItemText>Tournaments</ListItemText>
        </ListItem>
        <ListItem button component={Link} to="/guess-the-nation">
          <ListItemIcon>
            <Done />
          </ListItemIcon>
          <ListItemText>Guess The Nation Game</ListItemText>
        </ListItem>
        <Divider />
        {!decodedUser && <ListItem button component={Link} to="/sign-in">
          <ListItemIcon>
            <ExitToApp />
          </ListItemIcon>
          <ListItemText>Sign In</ListItemText>
        </ListItem>}
        {decodedUser && <ListItem button component={Link} to="/sign-out">
          <ListItemIcon>
            <DirectionsRun />
          </ListItemIcon>
          <ListItemText>Sign Out</ListItemText>
        </ListItem>}
      </List>
    </Drawer>
  )
}

export default Menu
