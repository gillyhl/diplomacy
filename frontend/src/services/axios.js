import axios from 'axios'
import getUserSession from './getUserSession'
export default () => axios.create({
  headers: {
    Authorization: `Bearer ${getUserSession()}`
  }
})