import getUserSession from './getUserSession'
import jwt_decode from 'jwt-decode'

export default () => {
  const jwt = getUserSession()
  if (!jwt) return false
  const decodedJwt = jwt_decode(jwt)
  return decodedJwt.exp > new Date().getTime() / 1000
}