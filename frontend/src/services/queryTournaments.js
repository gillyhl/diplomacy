import createAxios from 'services/axios'
import queryString from 'query-string'
export default value =>
  createAxios()
    .get(`/api/v1/tournament/query?${queryString.stringify({ q: value })}`)
    .then(res => res.data)
